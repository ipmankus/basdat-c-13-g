from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from toys_rent.helper.permission import admin_only
from toys_rent.helper import sql_wrapper as sql
from toys_rent.helper import jwt_wrapper as jwt
import json
import random

# Create your views here.
SORT = [
    'Unset',
    'Ascending order',
    'Descending order',
]

@require_http_methods(["GET"])
def item_form(request):
    query_kategori = "SELECT nama FROM KATEGORI;"
    kategori = []
    for cat in sql.fetch_all(query_kategori):
        kategori.append(cat["nama"])

    data = {
        'category': kategori
    }
    return render(request, 'main-pages/item-form.html', data)

def item_service(request):
    data = {
        'status': 500,
    }
    if (request.method == "GET"):
        query_kategori_item = "SELECT * FROM KATEGORI_ITEM;"
        items = sql.fetch_all(query_kategori_item)
        data = {
            'status': 200,
            'data': items, 
        }

    elif (request.method == "POST"):
        body = json.loads(request.body)
        if (body['usia_dari'] > body['usia_sampai']):
            data = {
                "status": 400,
                "message": "Age range must be consistent"
            }
        else:
            query_item = "INSERT INTO ITEM VALUES (%s, %s, %s, %s, %s);"
            sql.execute(
                query_item,
                body["nama"],
                body["deskripsi"],
                body["usia_dari"],
                body["usia_sampai"],
                body["bahan"],
            )

            query_kategori_item = "INSERT INTO KATEGORI_ITEM VALUES(%s, %s);"
            sql.execute(
                query_kategori_item,
                body["nama"],
                body["kategori"]
            )

            data = {
                'status': 200,
                'nama_item': body["nama"],
            }

    elif (request.method == 'PUT'):
        body = json.loads(request.body)
        query_item = "UPDATE ITEM\
            SET nama = %s,\
            SET deskripsi = %s,\
            SET usia_dari = %s,\
            SET usia_sampai = %s,\
            SET bahan = %s\
            WHERE nama = %s;"
            
        sql.execute(
            query_item,
            body["nama"],
            body["deskripsi"],
            body["usia_dari"],
            body["usia_sampai"],
            body["bahan"],
            body["nama_old"]
        )

        query_kategori_item = "UPDATE KATEGORI_ITEM\
            SET nama_item = %s,\
            SET nama_kategori = %s\
            WHERE nama_item = %s\
            AND nama_kategori = %s;"

        sql.execute(
            query_kategori_item,
            body["nama"],
            body["kategori"],
            body["nama_old"],
            body["kategori_old"]
        )

        data = {
            'status': 200,
            'nama_item': body["nama"],
        }
    return JsonResponse(data)

@admin_only
def delete_item_by_name(request, name):
    sql_kategori_item = "DELETE FROM KATEGORI_ITEM WHERE nama_item = %s"
    sql_item = "DELETE FROM ITEM WHERE nama = %s"
    sql.execute(sql_kategori_item, name)
    sql.execute(sql_item, name)
    return redirect('/stuff/item/')

def item_list(request):
    query_kategori_item = "SELECT DISTINCT nama_kategori FROM KATEGORI_ITEM;"
    kategori = []
    for cat in sql.fetch_all(query_kategori_item):
        kategori.append(cat["nama_kategori"])

    data = {
        'category': ["All"] + kategori,
        'sort': SORT,
    }
    return render(request, 'main-pages/item-display.html', data)

@admin_only
def create_barang(request):
    items = sql.fetch_all('SELECT nama FROM ITEM') 
    
    data = {
            'mock_user_name' : 'admin', 
            'mock_barang_list': [item['nama'] for item in items],
            'kondisi': [
                'Baru',
                'Bekas',
            ],
        }
    return render(request, 'main-pages/barang-form.html', data)

@admin_only
def create_barang_service(request):
    body = json.loads(request.body)
    response = JsonResponse({})
    try:
        response.status_code = 200
        item = sql.fetch_one("SELECT * FROM item WHERE nama=%s", body['nama_item'])
        penyewa = sql.fetch_one("SELECT * FROM anggota WHERE no_ktp=%s", body['no_ktp_penyewa'])
        if item == None or penyewa == None:
            raise Exception("item / penyewa ga ada")
        sql.execute("INSERT INTO BARANG VALUES (%s, %s, %s, %s, %s, %s, %s)", str(random.randint(0,1e10)), 
            body['nama_item'], body['warna'], body['url_foto'], body['kondisi'], body['lama_penggunaan'], body['no_ktp_penyewa'])
    except:
        response.status_code = 409
    return response
    
def show_barang(request):
    query = 'SELECT id_barang, nama_item, warna, nama_lengkap FROM barang,\
        pengguna WHERE no_ktp_penyewa = no_ktp'
    result = sql.fetch_all(query)
    data = {
        'table': result,
        'category': [
            'A',
            'B',
            'C'
        ],
        'sort': SORT,
    }
    return render(request, 'main-pages/barang-display.html', data)

def show_barang_with_id(request, id):
    barang = sql.fetch_one('SELECT * FROM barang WHERE id_barang=%s', id)
    if barang == None:
        return redirect('/stuff/barang/')
    else:        
        penyewa = sql.fetch_one('SELECT nama_lengkap FROM pengguna WHERE no_ktp=%s', barang['no_ktp_penyewa'])
        
        data = {
            'nama_item': barang['nama_item'],
            'url_foto': barang['url_foto'],
            'profile': [
                ('Warna', barang['warna']),
                ('Kondisi', barang['kondisi']),
                ('Lama Penggunaan', barang['lama_penggunaan']),
                ('Penyewa', penyewa['nama_lengkap']),
            ],
        }
        return render(request, 'main-pages/barang-display-detail.html', data)

@admin_only
def delete_barang_with_id(request, id):
    response = JsonResponse({})
    sql.execute('DELETE FROM barang WHERE id_barang=%s', id)
    return redirect('/stuff/barang/')
    