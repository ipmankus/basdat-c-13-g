from django.urls import path
from stuff import views

urlpatterns = [
    path('item/create/', views.item_form),
    path('item/api/', views.item_service),
    path('item/', views.item_list),
    path('item/delete/<name>', views.delete_item_by_name),
    path('barang/create/', views.create_barang),
    path('barang/create/api/', views.create_barang_service),
    path('barang/delete/<id>', views.delete_barang_with_id),
    path('barang/', views.show_barang),
    path('barang/<id>/', views.show_barang_with_id),
]