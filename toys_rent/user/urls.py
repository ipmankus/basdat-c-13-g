from django.urls import path
from user import views

urlpatterns = [
    path('login/', views.login_form),
    path('login/api/', views.login_service),
    path('logout/api/', views.logout_service),
    path('register/member/api/', views.member_service),
    path('register/admin/api/', views.admin_service),
    path('register/member/', views.member_form),
    path('register/admin/', views.admin_form),
    path('profile/', views.profile),
]