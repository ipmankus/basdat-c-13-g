from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from toys_rent.helper.permission import admin_only
from toys_rent.helper import sql_wrapper as sql
from toys_rent.helper import jwt_wrapper as jwt
from django.http import JsonResponse
import json


# Create your views here.
@require_http_methods(["GET"])
def login_form(request):
    session = jwt.decode(request.COOKIES.get('token'))
    data = {}
    if (session == None):
        return render(request, 'main-pages/login.html', data)
    else:
        return redirect('/user/profile')


@require_http_methods(["POST"])
def login_service(request):
    data = {
        'status': 500
    }
    response = JsonResponse(data)

    body = json.loads(request.body)
    query_login = 'SELECT * FROM PENGGUNA WHERE no_ktp = %s AND email = %s'
    
    user = sql.fetch_one(query_login, body['no_ktp'], body['email']) 
    if (user != None):
        query_check_admin = 'SELECT * FROM ADMIN WHERE no_ktp = %s'
        is_admin = False
        if (sql.fetch_one(query_check_admin, body['no_ktp']) != None):
            is_admin = True

        payload = {
            'no_ktp': user['no_ktp'],
            'is_admin': is_admin, 
            'nama': user['nama_lengkap']
        }

        data = {
            'status': 200,
        }
        response = JsonResponse(data)
        response.set_cookie('token', jwt.encode(payload))
    else:
        data = {
            'status': 409,
            'message': "No. KTP or Email doesn't fit"
        }
        response = JsonResponse(data)

    return response 

@require_http_methods(["GET"])
def logout_service(request):
    response = JsonResponse({
        "status": 200
    })
    
    response.delete_cookie('token')
    response.status_code = 200
    return response


@require_http_methods(["GET"])
def member_form(request):
    data = {}
    return render(request, 'main-pages/member-form.html', data)

@require_http_methods(["GET"])
def admin_form(request):
    data = {}
    return render(request, 'main-pages/admin-form.html', data)

@require_http_methods(["POST"])
def member_service(request): 
    body = json.loads(request.body)
    
    data = create_pengguna(body)
    if (data['status'] == 200):
        query_member = 'INSERT INTO ANGGOTA VALUES(%s, 0, null)'
        sql.execute(query_member, body['no_ktp'])

        print(body['semua_alamat'])
        for alamat in body['semua_alamat']:
            query_alamat = 'INSERT INTO ALAMAT VALUES(%s, %s, %s, %s, %s, %s)'
            sql.execute(
                query_alamat, 
                body['no_ktp'], 
                alamat['nama'], 
                alamat['jalan'], 
                alamat['nomor'], 
                alamat['kota'], 
                alamat['kode_pos']
            )
    return JsonResponse(data)

def create_pengguna(body):
    data = {
        'status': 400,
    }
        
    query_no_ktp = "SELECT no_ktp FROM PENGGUNA WHERE no_ktp = %s;"
    query_email = "SELECT email FROM PENGGUNA WHERE email = %s;"

    if (sql.fetch_one(query_no_ktp, body['no_ktp']) != None):
        data = {
            'status': 409,
            'message': 'No. KTP already exists'
        }
    elif (sql.fetch_one(query_email, body['email']) != None):
        data = {
            'status': 409,
            'message': 'Email already exists',
        }
    else:
        query_pengguna = "INSERT INTO PENGGUNA VALUES(%s, %s, %s, %s, %s);"

        sql.execute(
            query_pengguna, 
            body['no_ktp'],
            body['nama_lengkap'],
            body['email'],
            body['tanggal_lahir'],
            body['no_telp']
        );

        data = {
            'status': 200,
            'no_ktp': body['no_ktp'],
        }
    return data

@require_http_methods(["POST"])
def admin_service(request):
    body = json.loads(request.body)
    data = create_pengguna(body)
    if (data['status'] == 200 and request.method == 'POST'):
        query_admin = "INSERT INTO ADMIN VALUES(%s);"
        sql.execute(query_admin, body['no_ktp']);

    return JsonResponse(data);
    
@require_http_methods(["PUT"])
def update_pengguna(request):
    query_pengguna = "UPDATE PENGGUNA\
            SET nama_lengkap = %s,\
            SET email = %s,\
            SET tanggal_lahir = %s,\
            SET no_telp = %s\
            WHERE no_ktp = %s;"

    sql.execute(
        query_pengguna,
        body['nama_lengkap'],
        body['email'],
        body['tanggal_lahir'],
        body['no_telp'],
        body['no_ktp']
    )

    data = {
        'status': 200,
        'no_ktp': body['no_ktp'],
    }
    
    return data;

def profile(request):
    session = jwt.decode(request.COOKIES.get('token'))
    if session:
        query_profile = "SELECT * FROM PENGGUNA WHERE\
                no_ktp = %s"

        profile = sql.fetch_one(
            query_profile,
            session['no_ktp']
        )

        data = {
            'profile': [
                ('No KTP', profile['no_ktp']),
                ('Nama Lengkap', profile['nama_lengkap']),
                ('Email', profile['email']),
                ('Tanggal Lahir', profile['tanggal_lahir']),
                ('Nomor Telepon', profile['no_telp']),
            ]
        }

        if (not session['is_admin']):
            query_anggota = "SELECT * FROM ANGGOTA WHERE no_ktp = %s"
            anggota = sql.fetch_one(query_anggota, session['no_ktp'])

            query_alamat = "SELECT * FROM ALAMAT WHERE no_ktp_anggota = %s"
            alamat = sql.fetch_all(query_alamat, session['no_ktp'])
            data['member'] = [
                ('Level', anggota['level']),
                ('Poin', anggota['poin'])
            ]

            data['semua_alamat'] = alamat
        return render(request, 'main-pages/profile.html', data)
    else:
        return redirect('/')