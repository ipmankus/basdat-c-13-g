from django.shortcuts import redirect
from toys_rent.helper import sql_wrapper as sql
from toys_rent.helper import jwt_wrapper as jwt


def admin_only(func):
    def inner(request, *args, **kwargs):
        token = request.COOKIES.get('token')
        dec = jwt.decode(token)
        is_admin = sql.fetch_one('SELECT * FROM admin WHERE no_ktp = %s', dec['no_ktp']) 
        if is_admin:
            return func(request, *args, **kwargs)
        else:
            return redirect('/')
    return inner

