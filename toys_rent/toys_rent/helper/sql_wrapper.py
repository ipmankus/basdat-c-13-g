from django.db import connection


def execute(query, *args):
	with connection.cursor() as cursor:
		return cursor.execute(query, list(args))

def fetch_one(query, *args):
	with connection.cursor() as cursor:
		try:
			cursor.execute(query, list(args))
			row = cursor.fetchone()
			columns = [col[0] for col in cursor.description]
			return dict(zip(columns, row))
		except:
			return None

def fetch_all(query, *args):
	with connection.cursor() as cursor:
		try:
			cursor.execute(query, list(args))
			columns = [col[0] for col in cursor.description]
			return [
				dict(zip(columns, row))
				for row in cursor.fetchall()
			]
		except:
			return None

