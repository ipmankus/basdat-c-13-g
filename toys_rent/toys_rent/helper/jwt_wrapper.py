import jwt
from toys_rent.settings import SECRET_KEY

def encode(dic):
    return jwt.encode(dic, SECRET_KEY, algorithm='HS256').decode("utf-8") 

def decode(enc):
    if enc == None:
        return None
    return jwt.decode(enc, SECRET_KEY, algorithm=['HS256'])