$(document).ready(function() {
    let $message = $(this).find('.c-form__message');
    $('form').submit(function(e) {
        e.preventDefault();
        $message.attr('data-state', 'hide');

        let data = {
            "no_ktp": $(this).find("input[name='No. KTP']").val(),
            "email": $(this).find("input[name='Email']").val(),
        }
        
        $.ajax({
            url: '/user/login/api/',
            headers: {'X-CSRFToken': $(this).find("input[name='csrfmiddlewaretoken']").val()},
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(data),
            success: (result) => {
                checkResponse(result);
            }
        });
    });

    function checkResponse(result) {
        if (result.status == 409) {
            $message.attr('data-state', 'appear');
            $message.text(result.message);
        } else if (result.status === 200) {
            document.location = '/user/profile/'
        }
    }
});