function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = decodeURIComponent(atob(base64Url).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(base64);
};

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

$(document).ready(function() {
    $('#c-navbar__menu-button, #c-navbar__menu-back-button, #c-wall').on('click', function() {
        if ($('#c-navbar__menu-button').attr('data-state') === 'unclicked') {
            $('#l-section').attr('data-state', 'move-right');
            $('#l-navbar').attr('data-state', 'move-right');
            $('#l-menu').attr('data-state', 'move-right');
            $('#c-wall').attr('data-state', 'appear')
            $('#c-navbar__menu-button').attr('data-state', 'clicked');
        } else if ($('#c-navbar__menu-button').attr('data-state') === 'clicked') {
            $('#l-section').attr('data-state', 'idle');
            $('#l-navbar').attr('data-state', 'idle');
            $('#l-menu').attr('data-state', 'idle');
            $('#c-wall').attr('data-state', 'disappear')
            $('#c-navbar__menu-button').attr('data-state', 'unclicked');
        }
    });

    function filterPermission() {
        let session = parseJwt(getCookie('token'));
        if (session.no_ktp !== undefined) {
            $("*[data-permission='user']").attr('data-state', 'active');
            $auth = $("#l-navbar__auth");
            $auth.empty();
            $auth.append(`
                <a href='/user/profile/' class='l-auth l-flex--row-nowrap l-flex--center-center l-navbar__box'>
                    <h3>
                        PROFILE
                    </h3>
                </a>
                <button id='c-logout-button' class='l-auth l-flex--row-nowrap l-flex--center-center l-navbar__box'>
                    <h3>
                        LOGOUT
                    </h3>
                </button>
            `);
            if (session.is_admin) {
                $("*[data-permission='member']").attr('data-state', 'lost');
            } else {
                $("*[data-permission='admin']").attr('data-state', 'lost');
            }
        }
    }

    filterPermission();

    $("#l-navbar__auth").on("click", "#c-logout-button", () => {
        $.ajax({
            url: '/user/logout/api/',
            headers: {'X-CSRFToken': $(this).find("input[name='csrfmiddlewaretoken']").val()},
            type: 'GET',
            success: (response) => {
                if (response.status === 200) {
                    $auth = $("#l-navbar__auth")
                    $auth.empty();
                    $auth.append(`
                        <a href='/user/login/' class='l-auth l-flex--row-nowrap l-flex--center-center l-navbar__box'>
                            <h3>
                                REGISTER/LOGIN
                            </h3>
                        </a>
                    `);
                    window.location.href = '/user/login/'
                }
            }
        });
    });
});