$(document).ready(function() {
    $('#l-form__addresses').on('click', '#l-form__address-adder', function() {
        $(this).remove();
        $('#l-form__addresses').append(`
            <div class="l-form__multi-field l-form__address l-flex--row-nowrap l-flex--center-center">
                <input class='l-form__field h-animation-config' type='text' name='Tempat' placeholder='Tempat'>
                <input class='l-form__field h-animation-config' type='text' name='Jalan' placeholder='Jalan'>
                <input class='l-form__field h-animation-config' type='number' name='Nomor' placeholder='Nomor'>
                <input class='l-form__field h-animation-config' type='text' name='Kota' placeholder='Kota'>
                <input class='l-form__field h-animation-config' type='text' name='Kodepos' placeholder='Kodepos'>
                <button class='c-address-updater__circle-art l-flex--row-nowrap l-flex--center-center'>
                    -
                </button>
            </div>
            <button id='l-form__address-adder' class='l-flex--row-nowrap l-flex--center-center'>
                <div class='c-address-updater__circle-art l-flex--row-nowrap l-flex--center-center'>
                    +
                </div>
                <label>ALAMAT</label>
            </button>
        `);
    });

    $('#l-form__addresses').on('click', '.c-address-updater__circle-art', function() {
        $(this).parent().remove();
    });
});