$(document).ready(function() {
        var id = document.location.href.split('/')[document.location.href.split('/').length - 2];
        $('#l-admin__button').append(`

            <button id='l-update__barang' class='l-flex--row-nowrap l-flex--center-center' >
                <div class='c-address-updater__circle-art l-flex--row-nowrap l-flex--center-center'>
                    +
                </div>
                <label>UPDATE</label>
            </button>
            <button id='l-delete__barang' class='l-flex--row-nowrap l-flex--center-center' onclick="document.location='/stuff/barang/delete/`+id+`'">
                <div class='c-address-updater__circle-art l-flex--row-nowrap l-flex--center-center'>
                    +
                </div>
                <label>DELETE</label>
            </button>
        `);
});

