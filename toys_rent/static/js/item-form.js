$(document).ready(function() {
    let $message = $(this).find('.c-form__message');

    $('form').submit(function(e) {
        e.preventDefault();
        $message.attr('data-state', 'hide');

        const data = {
            "nama": $(this).find("input[name='Nama']").val(),
            "deskripsi": $(this).find("textarea[name='Deskripsi']").val(),
            "usia_dari": parseInt($(this).find("#l-form__age input[name='Usia Minimal']").val()),
            "usia_sampai": parseInt($(this).find("#l-form__age input[name='Usia Maksimal']").val()),
            "bahan": $(this).find("input[name='Bahan']").val(),
            "kategori": $(this).find("select[data-name='Kategori']").children("option:selected").val(),
        }

        $.ajax({
            url: '/stuff/item/api/',
            headers: {'X-CSRFToken': $(this).find("input[name='csrfmiddlewaretoken']").val()},
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(data),
            success: (response) => {
                checkResponse(response);
            }
        });
    });

    function checkResponse(result) {
        if (result.status === 409 || result.status === 400) {
            $message.attr('data-state', 'appear');
            $message.text(result.message);
        } else if (result.status === 200) {
            window.location.href = '/stuff/item/';
        }
    }
});