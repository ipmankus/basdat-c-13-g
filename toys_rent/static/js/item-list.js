$(document).ready(function() {
    const MAX_TUPLE = 50;
    var maxPage = undefined;
    var data = [];
    var page = 1;
    var cat = $("select[data-name='category']").children("option:selected").val();
    var sortType = $("select[data-name='sorter']").children("option:selected").val();
    var sortCol = "nama_item";

    function handleData() {
        let newData = []
        if (cat == "All") {
            newData = data;
        } else {
            newData = data.filter(function(obj) {
                return obj["nama_kategori"] === cat;
            });      
        }

        if (sortType === "Ascending order") {
            newData = data.sort((a, b) => (a[sortCol] > b[sortCol])? 1: -1)
        } else if (sortType === "Descending order") {
            newData = data.sort((a, b) => (a[sortCol] > b[sortCol])? -1: 1)
        }
        showData(newData);
    }

    function showData(data) {
        if (data.length > MAX_TUPLE * page) {
            data = data.slice((page - 1) * MAX_TUPLE, MAX_TUPLE * page);
        } else {
            data = data.slice((page - 1) * MAX_TUPLE, data.length);
        }

        let session = parseJwt(getCookie('token'));
        $("tbody").empty();
        let counter = 1;

        for (let item of data) {
            if (session.is_admin) {
                $("tbody").append(`
                <tr>
                    <td>
                        ${(page - 1) * MAX_TUPLE + counter++}
                    </td>
                    <td>
                        ${item.nama_item}
                    </td>
                    <td>
                        ${item.nama_kategori}
                    </td>
                    <td class='l-circle-art l-flex--row-nowrap l-flex--center-center' data-permission='admin'>
                        <a class='c-table__update-button c-circle-art l-flex--row-nowrap l-flex--center-center' href='/stuff/item/update/${item.nama_item}'>U</a>
                        <a class='c-table__delete-button c-circle-art l-flex--row-nowrap l-flex--center-center' href='/stuff/item/delete/${item.nama_item}'>D</a>
                    </td>
                </tr>
                `);
            } else {
                $("tbody").append(`
                <tr>
                    <td>
                        ${(page - 1) * MAX_TUPLE + counter++}
                    </td>
                    <td>
                        ${item.nama_item}
                    </td>
                    <td>
                        ${item.nama_kategori}
                    </td>
                </tr>
                `);   
            }
        }
    }

    $("#c-operation__prev-page").on("click", () => {
        if (page > 1) {
            page -= 1;
        }
        handleData();
    });

    $("#c-operation__next-page").on("click", () => {
        if (data.slice((page) * MAX_TUPLE, (page + 1) * MAX_TUPLE)[0] !== undefined) {
            page += 1;
        }
        handleData();
    });

    $.ajax({
        url: '/stuff/item/api/',
        headers: {'X-CSRFToken': $(this).find("input[name='csrfmiddlewaretoken']").val()},
        type: 'GET',
        success: (response) => {
            if (response.status == 200) {
                data = response.data;
                maxPage = Math.ceil(data / MAX_TUPLE);
                handleData();
            }
        }
    });
});