$(document).ready(function() {
    let $message = $(this).find('.c-form__message');
    $('form').submit(function(e) {
        e.preventDefault();
        $message.attr('data-state', 'hide');

        let data = {
            "no_ktp": $(this).find("input[name='No. KTP']").val(),
            "nama_lengkap": $(this).find("input[name='Nama Lengkap']").val(),
            "email": $(this).find("input[name='Email']").val(),
            "tanggal_lahir": $(this).find("input[name='Tanggal Lahir']").val(),
            "no_telp": $(this).find("input[name='Nomor Telepon']").val(),
        }

        if ($(this).attr('data-user') === 'ADMIN') {
            $.ajax({
                url: '/user/register/admin/api/',
                headers: {'X-CSRFToken': $(this).find("input[name='csrfmiddlewaretoken']").val()},
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(data),
                success: (result) => {
                    checkResponse(result);
                }
            });

        } else if ($(this).attr('data-user') === 'MEMBER') {
            data['semua_alamat'] = []
            $('#l-form__addresses .l-form__multi-field').each(function() {
                console.log($(this).find("input[name='Tempat']").val())
                const alamat = {
                    nama: $(this).find("input[name='Tempat']").val(),
                    jalan: $(this).find("input[name='Jalan']").val(),
                    nomor: $(this).find("input[name='Nomor']").val(),
                    kota: $(this).find("input[name='Kota']").val(),
                    kode_pos: $(this).find("input[name='Kodepos']").val(),
                }
                data['semua_alamat'].push(alamat);
            })

            $.ajax({
                url: '/user/register/member/api/',
                headers: {'X-CSRFToken': $(this).find("input[name='csrfmiddlewaretoken']").val()},
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(data),
                success: (result) => {
                    checkResponse(result);
                }
            });
        }
    });

    function checkResponse(result) {
        if (result.status == 409) {
            $message.attr('data-state', 'appear');
            $message.text(result.message);
        }
    }
});