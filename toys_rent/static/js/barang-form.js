$(document).ready(function() {
    $('form').submit(function(e) {
        e.preventDefault();
       
        let data = {
            "nama_item": $(this).find("select[data-name='Nama Item']").children("option:selected").val(),
            "warna": $(this).find("input[name='Warna']").val(),
            "url_foto": $(this).find("input[name='Url Foto']").val(),
            "kondisi": $(this).find("select[data-name='Kondisi']").children("option:selected").val(),
            "lama_penggunaan": $(this).find("input[name='Lama Peggunaan (hari)']").val(),
            "no_ktp_penyewa": $(this).find("input[name='No. KTP Penyewa']").val(),
        }
        
        $.ajax({
            url: '/stuff/barang/create/api/',
            headers: {'X-CSRFToken': $(this).find("input[name='csrfmiddlewaretoken']").val()},
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(data),
            success: (result) => {

                checkResponse(result);
            },
            error: (result) => {

                checkResponse(result);
            }
        });

        
    });

    function checkResponse(result) {

        if (result.status == 409) {
            swal("Oh noes!", "Something went wrong :(", "error");
        } else {
            swal("Nice!", "Stuff was successfully added", "success");
        }
    }
});