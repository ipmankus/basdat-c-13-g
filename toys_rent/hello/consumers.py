from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from toys_rent.helper import sql_wrapper as sql
import json
import random
import datetime


class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )
        no_ktp_anggota = self.room_name.split('-')[0]
        no_ktp_admin = self.room_name.split('-')[1]
        
        sql.execute('INSERT INTO chat values(%s, %s, %s, %s, %s)',
            str(random.randint(0,1e15)), message, datetime.datetime.now(), 
            no_ktp_anggota, no_ktp_admin)


    # Receive message from room group
    def chat_message(self, event):
        message = event['message']
    
        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))