
from django.urls import path

from . import views

urlpatterns = [
    path('<slug:no_ktp_1>/', views.room, name='room'),
]