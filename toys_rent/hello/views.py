from django.shortcuts import render, redirect
from django.utils.safestring import mark_safe
import toys_rent.helper.jwt_wrapper as jwt
import toys_rent.helper.sql_wrapper as sql
import json

def index(request):
    return redirect('/')

def room(request, no_ktp_1):
    # test pakai user 1234321 dan 12341234
    token = request.COOKIES.get('token')
    if token:
        dec = jwt.decode(token)
        no_ktp_2 = dec['no_ktp']
        
        is_admin1 = sql.fetch_one('SELECT * FROM admin WHERE no_ktp=%s', no_ktp_1)
        is_admin2 = sql.fetch_one('SELECT * FROM admin WHERE no_ktp=%s', no_ktp_2)
        no_ktp_admin = no_ktp_anggota = None

        if is_admin2 and is_admin1:
            return redirect('/')
        elif not is_admin1 and not is_admin2:
            return redirect('/')
        elif is_admin1:
            no_ktp_admin, no_ktp_anggota = no_ktp_1, no_ktp_2
        else:
            no_ktp_admin, no_ktp_anggota = no_ktp_2, no_ktp_1
        
        res = sql.fetch_all('SELECT * FROM CHAT where no_ktp_anggota=%s AND no_ktp_admin=%s \
            ORDER BY date_time ASC', 
            no_ktp_anggota, no_ktp_admin)

        return render(request, 'room.html', {
            'room_name_json': mark_safe(json.dumps('{}-{}'.format(no_ktp_anggota, no_ktp_admin))),
            'chats': res
        })
    else:
        return redirect('/user/login')