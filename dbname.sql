--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;

--
-- Name: test; Type: SCHEMA; Schema: -; Owner: db2018041
--

CREATE SCHEMA test;


ALTER SCHEMA test OWNER TO db2018041;

--
-- Name: toys_rent; Type: SCHEMA; Schema: -; Owner: db2018041
--

CREATE SCHEMA toys_rent;


ALTER SCHEMA toys_rent OWNER TO db2018041;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: update_poin_anggota_on_pemesanan(); Type: FUNCTION; Schema: public; Owner: db2018041
--

CREATE FUNCTION public.update_poin_anggota_on_pemesanan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        UPDATE ANGGOTA SET poin = poin + NEW.kuantitas_barang * 100;
    END;
$$;


ALTER FUNCTION public.update_poin_anggota_on_pemesanan() OWNER TO db2018041;

--
-- Name: update_poin_anggota_on_barang(); Type: FUNCTION; Schema: toys_rent; Owner: db2018041
--

CREATE FUNCTION public.update_poin_anggota_on_barang() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        UPDATE ANGGOTA a SET poin = poin + 100 WHERE NEW.no_ktp_penyewa = a.no_ktp;
        RETURN NEW;
    END;
$$;


ALTER FUNCTION update_poin_anggota_on_barang() OWNER TO db2018041;

--
-- Name: update_poin_anggota_on_pemesanan(); Type: FUNCTION; Schema: toys_rent; Owner: db2018041
--

CREATE OR REPLACE FUNCTION public.update_poin_anggota_on_pemesanan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        UPDATE ANGGOTA a SET poin = poin + NEW.kuantitas_barang * 100 WHERE NEW.no_ktp_pemesan = a.no_ktp;
        RETURN NEW;
    END;
$$;


ALTER FUNCTION update_poin_anggota_on_pemesanan() OWNER TO db2018041;

--
-- Name: update_user_level_when_level_update(); Type: FUNCTION; Schema: toys_rent; Owner: db2018041
--

CREATE OR REPLACE FUNCTION update_user_level_when_level_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
        anggota RECORD;
        level RECORD;
    BEGIN
        IF(TG_OP = 'UPDATE' AND NEW.minimum_poin = OLD.minimum_poin) THEN
            RETURN NEW;
        END IF;

        FOR anggota IN 
            SELECT no_ktp, poin
            FROM ANGGOTA
        LOOP
            FOR level IN 
                SELECT nama_level, minimum_poin
                FROM LEVEL_KEANGGOTAAN
                ORDER BY minimum_poin DESC
            LOOP
                IF (level.minimum_poin <= anggota.poin) THEN
                    UPDATE ANGGOTA A
                    SET level = level.nama_level
                    WHERE A.no_ktp = anggota.no_ktp;
                END IF;
            END LOOP;
        END LOOP;

        IF (TG_OP = 'DELETE') THEN
            RETURN OLD;
        ELSE
            RETURN NEW;
        END IF;
    END;
$$;


ALTER FUNCTION update_user_level_when_level_update() OWNER TO db2018041;

--
-- Name: update_user_level_when_user_update(); Type: FUNCTION; Schema: toys_rent; Owner: db2018041
--

CREATE FUNCTION .update_user_level_when_user_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
        rec RECORD;
    BEGIN
        IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
            IF (NEW.poin < 0) THEN
                RAISE EXCEPTION 'can''t be negative number';
            END IF;

            FOR rec IN 
                SELECT nama_level, minimum_poin
                FROM LEVEL_KEANGGOTAAN
                ORDER BY minimum_poin DESC
            LOOP
                IF (rec.minimum_poin <= NEW.poin) THEN
                    NEW.level := rec.nama_level;
                    RETURN NEW; 
                END IF;
            END LOOP;
        END IF;
    END;
$$;


ALTER FUNCTION update_user_level_when_user_update() OWNER TO db2018041;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE admin (
    no_ktp character varying(20) NOT NULL
);


ALTER TABLE admin OWNER TO db2018041;

--
-- Name: alamat; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE alamat (
    no_ktp_anggota character varying(15) NOT NULL,
    nama character varying(255) NOT NULL,
    jalan character varying(255) NOT NULL,
    nomor integer NOT NULL,
    kota character varying(255) NOT NULL,
    kodepos character varying(10) NOT NULL
);


ALTER TABLE alamat OWNER TO db2018041;

--
-- Name: anggota; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE anggota (
    no_ktp character varying(20) NOT NULL,
    poin real NOT NULL,
    level character varying(20)
);


ALTER TABLE anggota OWNER TO db2018041;

--
-- Name: barang; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE barang (
    id_barang character varying(10) NOT NULL,
    nama_item character varying(255),
    warna character varying(50),
    url_foto text,
    kondisi text,
    lama_penggunaan integer,
    no_ktp_penyewa character varying(20),
    CONSTRAINT barang_kondisi_check CHECK ((kondisi IS NOT NULL)),
    CONSTRAINT barang_nama_item_check CHECK ((nama_item IS NOT NULL)),
    CONSTRAINT barang_no_ktp_penyewa_check CHECK ((no_ktp_penyewa IS NOT NULL))
);


ALTER TABLE barang OWNER TO db2018041;

--
-- Name: barang_dikembalikan; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE barang_dikembalikan (
    no_resi character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10)
);


ALTER TABLE .barang_dikembalikan OWNER TO db2018041;

--
-- Name: barang_dikirim; Type: TABLE; Schema: ; Owner: db2018041; Tablespace: 
--

CREATE TABLE barang_dikirim (
    no_resi character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10),
    tanggal_review date NOT NULL,
    review text NOT NULL
);


ALTER TABLE barang_dikirim OWNER TO db2018041;

--
-- Name: barang_pesanan; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE barang_pesanan (
    id_pemesanan character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10),
    tanggal_sewa date NOT NULL,
    lama_sewa integer NOT NULL,
    tanggal_kembali date,
    status character varying(50) NOT NULL
);


ALTER TABLE barang_pesanan OWNER TO db2018041;

--
-- Name: chat; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

# postgres://jkpngquyqsupan:d29f08ce534b7c768934bb6136263a0d9169ef9251bdfc5460a59a7309fcaf43@ec2-54-221-201-212.compute-1.amazonaws.com:5432/d8qsm85f9ncl8a

CREATE TABLE chat (
    id character varying(15) NOT NULL,
    pesan text NOT NULL,
    date_time timestamp without time zone NOT NULL,
    no_ktp_anggota character varying(20) NOT NULL,
    no_ktp_admin character varying(20) NOT NULL
);


ALTER TABLE chat OWNER TO db2018041;

--
-- Name: info_barang_level; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE info_barang_level (
    id_barang character varying(10) NOT NULL,
    nama_level character varying(20) NOT NULL,
    harga_sewa real,
    porsi_royalti real,
    CONSTRAINT info_barang_level_harga_sewa_check CHECK ((harga_sewa IS NOT NULL)),
    CONSTRAINT info_barang_level_porsi_royalti_check CHECK ((porsi_royalti IS NOT NULL))
);


ALTER TABLE info_barang_level OWNER TO db2018041;

--
-- Name: item; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE item (
    nama character varying(255) NOT NULL,
    deskripsi text,
    usia_dari integer,
    usia_sampai integer,
    bahan text,
    CONSTRAINT item_usia_dari_check CHECK ((usia_dari IS NOT NULL)),
    CONSTRAINT item_usia_sampai_check CHECK ((usia_sampai IS NOT NULL))
);


ALTER TABLE item OWNER TO db2018041;

--
-- Name: kategori; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE kategori (
    nama character varying(255) NOT NULL,
    level integer,
    sub_dari character varying(255),
    CONSTRAINT kategori_level_check CHECK ((level IS NOT NULL))
);


ALTER TABLE kategori OWNER TO db2018041;

--
-- Name: kategori_item; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE kategori_item (
    nama_item character varying(255) NOT NULL,
    nama_kategori character varying(255) NOT NULL
);


ALTER TABLE kategori_item OWNER TO db2018041;

--
-- Name: level_keanggotaan; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE level_keanggotaan (
    nama_level character varying(50) NOT NULL,
    minimum_poin real NOT NULL,
    deskripsi text
);


ALTER TABLE level_keanggotaan OWNER TO db2018041;

--
-- Name: pemesanan; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE pemesanan (
    id_pemesanan character varying(10) NOT NULL,
    datetime_pesanan timestamp without time zone NOT NULL,
    kuantitas_barang integer NOT NULL,
    harga_sewa real,
    ongkos real,
    no_ktp_pemesan character varying(20) NOT NULL,
    status character varying(50)
);


ALTER TABLE pemesanan OWNER TO db2018041;

--
-- Name: pengembalian; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE pengembalian (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20),
    nama_alamat_anggota character varying(255)
);


ALTER TABLE pengembalian OWNER TO db2018041;

--
-- Name: pengguna; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE pengguna (
    no_ktp character varying(20) NOT NULL,
    nama_lengkap character varying(255) NOT NULL,
    tanggal_lahir date,
    no_telp character varying(20)
);


ALTER TABLE pengguna OWNER TO db2018041;

--
-- Name: pengiriman; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE pengiriman (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20),
    nama_alamat_anggota character varying(255)
);


ALTER TABLE pengiriman OWNER TO db2018041;

--
-- Name: status; Type: TABLE; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

CREATE TABLE status (
    nama character varying(50) NOT NULL,
    deskripsi text
);


ALTER TABLE status OWNER TO db2018041;

--
-- Data for Name: admin; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY admin (no_ktp) FROM stdin;
91
92
93
94
95
96
97
98
99
100
\.


--
-- Data for Name: alamat; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY alamat (no_ktp_anggota, nama, jalan, nomor, kota, kodepos) FROM stdin;
1	Kosan	6th	91	Sukowono	17715
2	Kontrakan	Macpherson	59	Sotouboua	18025
3	Apartment	Scott	25	Vale Maceiras	13374
4	Kontrakan	Vernon	69	Halmstad	16042
5	Kontrakan	Clarendon	30	Louisville	12632
6	Kosan	Sullivan	14	Cortinhas	17391
7	Kosan	Graedel	13	Bieligutai	12207
8	Apartment	Upham	62	Ed	16914
9	Kontrakan	Heath	91	Valinhos	12815
10	Kantor	Hansons	27	Witzenhausen	18284
11	Kantor	Dapin	83	Yên Lập	15281
12	Kantor	Lyons	90	München	13042
13	Apartment	Becker	75	Hukeng	13222
14	Toko	Commercial	81	Debre Zeyit	17776
15	Kontrakan	Fallview	86	Río Limpio	15606
16	Kontrakan	Kropf	1	Ilama	12632
17	Kosan	Stuart	87	Youcheng	16459
18	Toko	Sutteridge	40	José Bonifácio	14587
19	Apartment	Katie	20	San Simón	12084
20	Toko	Mandrake	94	Zonghan	18466
21	Kontrakan	Rigney	5	Poroshkovo	12121
22	Rumah	Waxwing	34	Osasco	14451
23	Rumah	Mallory	57	Longhe	12140
24	Toko	Magdeline	63	Kangalassy	15755
25	Kontrakan	Lake View	13	Santo Domingo	17168
26	Rumah	Linden	86	Gowarczów	16406
27	Toko	High Crossing	64	Palocabildo	15777
28	Apartment	Luster	97	Yuzhai	18757
29	Kosan	Union	16	San Jose	15275
30	Toko	Barby	72	Polomolok	13642
31	Apartment	Paget	27	Borabue	12771
32	Kantor	Spaight	45	Erie	18580
33	Rumah	West	33	Lleida	14647
34	Kosan	Maple Wood	91	Colcabamba	16809
35	Kontrakan	Ridge Oak	27	Makati City	16892
36	Kosan	Independence	10	Plottier	18944
37	Kontrakan	Reinke	94	Boñgalon	13777
38	Apartment	Corscot	81	Solna	13493
39	Toko	Merchant	88	Messíni	15889
40	Kosan	Arapahoe	16	Shanling	17643
41	Kosan	Northridge	74	Pesisir	13954
42	Kantor	Green Ridge	90	Hekou	17155
43	Rumah	Sutherland	49	Abdurahmoni Jomí	15115
44	Apartment	Huxley	79	Baorixile	17284
45	Toko	Columbus	63	La Suiza	17624
46	Kosan	Banding	24	Daytona Beach	13161
47	Kantor	Anderson	86	Ath	16663
48	Kosan	Ludington	84	Kaiyun	18272
49	Rumah	Fulton	46	Fika	14503
50	Kantor	Anderson	42	Semiletka	12524
51	Kantor	Susan	46	Sidi Bousber	15602
52	Toko	Helena	42	Iporá	16833
53	Kontrakan	Maple	53	Buganda	12818
54	Kosan	Village	48	Mo I Rana	15061
55	Kosan	Surrey	24	Sedan	15847
56	Kontrakan	Declaration	74	Novorossiysk	18404
57	Toko	Basil	27	Xinshiba	16282
58	Kosan	Garrison	16	Montauban	18245
59	Apartment	Eastlawn	31	Bergen	16365
60	Kosan	Del Mar	39	Cengjia	14641
61	Apartment	Blaine	75	Ramos West	12137
62	Kontrakan	Surrey	67	Brondong	14555
63	Toko	Red Cloud	2	Copa	13752
64	Kantor	Nevada	89	Stockholm	15953
65	Kosan	Sachs	74	Mohammedia	15073
66	Toko	Clemons	23	Yihe	13659
67	Kantor	Forster	68	Wasior	12412
68	Toko	Nova	48	Simnas	14340
69	Toko	Springview	32	Machagai	13726
70	Kontrakan	Karstens	49	Entradas	15430
71	Kosan	Dawn	42	Citeureup	16333
72	Kantor	Monument	96	Banjar Parekan	16388
73	Rumah	Fairview	4	Bigaa	18239
74	Kontrakan	Almo	72	Liuhou	12780
75	Kontrakan	Schmedeman	95	La Concepcion	13485
76	Kosan	Sullivan	40	Ciguha Tengah	15240
77	Kantor	Talisman	22	Keleng	18199
78	Rumah	Veith	19	Orlando	13714
79	Rumah	Esker	18	Simuay	18034
80	Kontrakan	Village Green	64	Dabaozi	13157
81	Toko	Fuller	51	Trondheim	15727
82	Kosan	Claremont	55	Yayao	15214
83	Apartment	Vahlen	92	Cidamar	15957
84	Toko	Fisk	81	Remas	12964
85	Toko	Rutledge	51	Mihara	12312
86	Toko	Shopko	13	Diourbel	14629
87	Rumah	Northfield	43	Listvyanka	15606
88	Apartment	Vidon	8	Portmore	12181
89	Kontrakan	Mesta	29	Bojongbenteng	17780
90	Apartment	Pleasure	35	Gaigeturi	13683
1	Apartment	Center	9	San Gil	16147
2	Rumah	Susan	29	Sakai-nakajima	15554
3	Rumah	Forest Dale	41	Kangalassy	14162
4	Kosan	Porter	100	Ţahţā	12025
5	Toko	Clove	62	Al Matūn	16463
6	Apartment	2nd	24	Duma	16560
7	Rumah	Algoma	19	Cisaat	18826
8	Kantor	Alpine	19	Gensi	16270
9	Kantor	Melrose	78	Uppsala	16614
10	Kosan	Almo	18	Bua Yai	18283
\.


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY anggota (no_ktp, poin, level) FROM stdin;
12	69017	Bronze
13	600108	Gold
14	436193	Mythical
15	680500	Gold
16	949293	Gold
17	36257	Bronze
18	847115	Gold
19	441119	Mythical
20	584695	Gold
21	759033	Gold
22	523732	Gold
23	582147	Gold
24	411881	Mythical
25	267792	Silver
26	670962	Gold
27	690878	Gold
28	582202	Gold
29	859412	Gold
30	103149	Silver
31	382127	Mythical
32	122048	Silver
33	884021	Gold
34	928700	Gold
35	932210	Gold
36	102019	Silver
37	366844	Mythical
38	228196	Silver
39	978869	Gold
40	173667	Silver
41	204011	Silver
42	105931	Silver
1	768100	Gold
2	203708	Silver
3	31582	Bronze
4	268350	Silver
5	453532	Mythical
6	595611	Gold
7	812994	Gold
8	148633	Silver
9	481566	Mythical
10	664120	Gold
11	199179	Silver
43	514772	Gold
64	890971	Gold
44	565220	Gold
45	444519	Mythical
46	572185	Gold
47	953576	Gold
48	51134	Bronze
49	424921	Mythical
50	522883	Gold
51	213120	Silver
52	486639	Mythical
53	124509	Silver
54	867720	Gold
55	691096	Gold
56	598180	Gold
57	259130	Silver
58	648105	Gold
59	698611	Gold
60	195878	Silver
61	914482	Gold
62	171047	Silver
63	349480	Mythical
65	902069	Gold
66	480109	Mythical
67	461062	Mythical
68	628584	Gold
69	265823	Silver
70	804834	Gold
71	899798	Gold
72	710063	Gold
73	157401	Silver
74	313248	Mythical
75	796144	Gold
76	314254	Mythical
77	796908	Gold
78	931098	Gold
79	645804	Gold
80	212836	Silver
81	626401	Gold
82	237368	Silver
83	558726	Gold
84	592104	Gold
85	353760	Mythical
86	785100	Gold
87	178569	Silver
88	782010	Gold
89	512955	Gold
90	926260	Gold
3679213	6000	Bronze
3679212	120000	Silver
3679211	860000	Gold
\.


--
-- Data for Name: barang; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY barang (id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, no_ktp_penyewa) FROM stdin;
1	A5	Finnigan	http://dummyimage.com/152x207.bmp/cc0000/ffffff	usable	58	1
2	Impreza	Sebrens	http://dummyimage.com/224x128.bmp/5fa2dd/ffffff	usable	35	4
3	GTI	Taysbil	http://dummyimage.com/247x219.jpg/ff4444/ffffff	bad	5	5
4	Quattroporte	Francis	http://dummyimage.com/183x174.bmp/5fa2dd/ffffff	good	29	5
5	Legacy	Ridgley	http://dummyimage.com/103x156.jpg/cc0000/ffffff	usable	39	1
6	Tundra	Stopp	http://dummyimage.com/236x143.png/dddddd/000000	bad	58	90
7	Grand Prix	Pomphrett	http://dummyimage.com/201x180.jpg/cc0000/ffffff	bad	77	33
8	Tundra	Raffin	http://dummyimage.com/103x224.bmp/ff4444/ffffff	usable	24	44
9	A5	Bastone	http://dummyimage.com/143x228.jpg/dddddd/000000	bad	28	13
10	Corvette	Meekin	http://dummyimage.com/230x102.jpg/5fa2dd/ffffff	good	35	13
11	Firebird	Duxbarry	http://dummyimage.com/179x132.jpg/ff4444/ffffff	supreme	24	13
12	Tucson	Dobbson	http://dummyimage.com/149x118.jpg/ff4444/ffffff	bad	33	13
13	Exige	Diggons	http://dummyimage.com/156x160.jpg/cc0000/ffffff	good	83	13
14	F-Series	Woollard	http://dummyimage.com/222x247.png/cc0000/ffffff	usable	82	13
15	Legacy	Chominski	http://dummyimage.com/170x149.jpg/ff4444/ffffff	bad	50	13
16	Quattroporte	Lescop	http://dummyimage.com/212x147.jpg/5fa2dd/ffffff	bad	81	13
17	Tucson	Bastiman	http://dummyimage.com/151x162.bmp/ff4444/ffffff	good	75	13
18	Corvette	Oakshott	http://dummyimage.com/188x198.png/5fa2dd/ffffff	usable	40	13
19	Legacy	Barti	http://dummyimage.com/230x219.bmp/cc0000/ffffff	usable	32	13
20	Vibe	Gilcriest	http://dummyimage.com/239x134.jpg/dddddd/000000	usable	12	13
21	G-Class	Malbon	http://dummyimage.com/181x218.png/cc0000/ffffff	supreme	80	13
22	TSX	Simonsen	http://dummyimage.com/165x194.jpg/5fa2dd/ffffff	supreme	55	13
23	Vibe	Lemoir	http://dummyimage.com/126x138.jpg/ff4444/ffffff	usable	78	13
24	TSX	Coldbreath	http://dummyimage.com/162x147.bmp/ff4444/ffffff	supreme	88	71
25	Vitara	Abramowitch	http://dummyimage.com/240x201.png/dddddd/000000	usable	84	71
26	Villager	Gertz	http://dummyimage.com/101x100.jpg/ff4444/ffffff	good	80	71
27	3 Series	Bleby	http://dummyimage.com/211x109.png/ff4444/ffffff	supreme	27	71
28	Corvette	McShane	http://dummyimage.com/244x108.jpg/ff4444/ffffff	supreme	9	71
29	Exige	Hardison	http://dummyimage.com/193x199.jpg/dddddd/000000	usable	16	71
30	G-Class	Wilce	http://dummyimage.com/141x204.png/5fa2dd/ffffff	usable	78	71
31	Villager	Cowlard	http://dummyimage.com/138x173.png/cc0000/ffffff	usable	27	71
32	Vitara	Rowlinson	http://dummyimage.com/125x162.bmp/5fa2dd/ffffff	good	34	71
33	TL	Knightly	http://dummyimage.com/187x130.jpg/5fa2dd/ffffff	bad	30	71
34	Grand Prix	Veldens	http://dummyimage.com/135x166.bmp/dddddd/000000	bad	28	71
35	F-Series	Bowie	http://dummyimage.com/179x152.bmp/dddddd/000000	bad	1	71
36	H2	Piotrowski	http://dummyimage.com/238x162.jpg/5fa2dd/ffffff	supreme	1	71
37	TSX	Sculpher	http://dummyimage.com/122x193.png/5fa2dd/ffffff	good	44	71
38	Grand Prix	Chinge	http://dummyimage.com/124x128.png/cc0000/ffffff	bad	11	71
39	Vibe	Gracey	http://dummyimage.com/183x184.png/dddddd/000000	bad	41	45
40	Tucson	Leamy	http://dummyimage.com/226x128.bmp/ff4444/ffffff	good	50	45
41	Firebird	Whytock	http://dummyimage.com/207x242.png/ff4444/ffffff	good	19	45
42	Vitara	Tinton	http://dummyimage.com/226x205.jpg/dddddd/000000	supreme	35	45
43	Land Cruiser	Eisikowitch	http://dummyimage.com/242x175.bmp/dddddd/000000	good	52	45
44	Exige	Vicioso	http://dummyimage.com/205x121.jpg/5fa2dd/ffffff	supreme	33	45
45	X5	Kibbee	http://dummyimage.com/219x153.jpg/ff4444/ffffff	good	12	45
46	Fleetwood	Pedrol	http://dummyimage.com/104x180.jpg/cc0000/ffffff	supreme	1	45
47	Legacy	Aubrey	http://dummyimage.com/129x242.bmp/cc0000/ffffff	supreme	22	45
48	3 Series	Milam	http://dummyimage.com/122x241.png/ff4444/ffffff	supreme	7	45
49	Quattroporte	Statter	http://dummyimage.com/157x240.jpg/5fa2dd/ffffff	bad	85	14
50	F-Series	Duerden	http://dummyimage.com/114x204.bmp/cc0000/ffffff	usable	64	14
51	Vibe	Huckett	http://dummyimage.com/145x107.png/5fa2dd/ffffff	supreme	71	19
52	Grand Prix	Ramelot	http://dummyimage.com/175x137.png/cc0000/ffffff	bad	52	19
53	Vitara	Grunder	http://dummyimage.com/194x241.png/cc0000/ffffff	usable	22	19
54	GTI	Rainard	http://dummyimage.com/137x186.jpg/dddddd/000000	good	89	19
55	GTI	Denson	http://dummyimage.com/205x242.bmp/5fa2dd/ffffff	usable	13	19
56	Legacy	Regitz	http://dummyimage.com/136x145.jpg/ff4444/ffffff	supreme	37	19
57	TSX	Farndell	http://dummyimage.com/133x240.bmp/dddddd/000000	usable	15	19
58	Vibe	Mayou	http://dummyimage.com/229x105.bmp/cc0000/ffffff	bad	35	19
59	GTI	Riglesford	http://dummyimage.com/226x123.jpg/dddddd/000000	bad	33	19
60	G-Class	Itscowics	http://dummyimage.com/167x179.jpg/dddddd/000000	bad	90	31
61	Land Cruiser	Oene	http://dummyimage.com/132x194.bmp/ff4444/ffffff	supreme	29	31
62	TSX	Menendez	http://dummyimage.com/179x230.bmp/cc0000/ffffff	supreme	12	31
63	H2	Ceney	http://dummyimage.com/169x180.png/cc0000/ffffff	good	66	31
64	Impreza	Kinsett	http://dummyimage.com/118x141.png/dddddd/000000	bad	24	31
65	Firebird	Ianno	http://dummyimage.com/120x224.bmp/ff4444/ffffff	good	58	31
66	GTI	Andreopolos	http://dummyimage.com/111x244.bmp/dddddd/000000	usable	10	31
67	Vitara	O'Sheils	http://dummyimage.com/115x154.bmp/5fa2dd/ffffff	bad	28	31
68	Villager	Etter	http://dummyimage.com/161x189.png/cc0000/ffffff	usable	55	31
69	Grand Prix	Simonsen	http://dummyimage.com/136x161.png/dddddd/000000	supreme	59	74
70	TSX	Hedau	http://dummyimage.com/183x180.png/dddddd/000000	usable	86	74
71	Tundra	Taffie	http://dummyimage.com/104x112.bmp/cc0000/ffffff	bad	50	74
72	Tundra	Verlander	http://dummyimage.com/119x146.bmp/cc0000/ffffff	good	56	74
73	Firebird	Meller	http://dummyimage.com/184x111.png/ff4444/ffffff	good	7	74
74	Land Cruiser	Folomkin	http://dummyimage.com/238x112.png/5fa2dd/ffffff	bad	41	74
75	Villager	Yorkston	http://dummyimage.com/151x142.bmp/ff4444/ffffff	bad	41	74
76	TL	Andrichuk	http://dummyimage.com/144x216.png/cc0000/ffffff	supreme	19	74
77	Legacy	Leavens	http://dummyimage.com/118x168.jpg/cc0000/ffffff	bad	46	74
78	H2	Fareweather	http://dummyimage.com/218x233.bmp/ff4444/ffffff	bad	36	74
79	Quattroporte	Vynall	http://dummyimage.com/186x207.png/ff4444/ffffff	good	56	74
80	Legacy	Pogosian	http://dummyimage.com/107x243.bmp/cc0000/ffffff	bad	16	74
81	H2	Willcocks	http://dummyimage.com/156x237.jpg/5fa2dd/ffffff	good	76	74
82	Vitara	Amaya	http://dummyimage.com/179x164.jpg/5fa2dd/ffffff	supreme	11	74
83	Fleetwood	Haggleton	http://dummyimage.com/226x144.png/ff4444/ffffff	supreme	17	81
84	Esprit	Dodle	http://dummyimage.com/169x128.png/5fa2dd/ffffff	usable	7	81
85	Impreza	Malarkey	http://dummyimage.com/220x131.bmp/cc0000/ffffff	bad	89	81
86	Corvette	McDonagh	http://dummyimage.com/133x231.bmp/cc0000/ffffff	bad	12	81
87	A5	Rebbeck	http://dummyimage.com/122x162.png/5fa2dd/ffffff	usable	48	81
88	Vitara	Foyster	http://dummyimage.com/151x156.jpg/5fa2dd/ffffff	good	89	51
89	Impreza	Exton	http://dummyimage.com/196x195.png/cc0000/ffffff	good	49	61
90	Quattroporte	Dallemore	http://dummyimage.com/163x241.png/cc0000/ffffff	supreme	16	44
91	Esprit	Loughan	http://dummyimage.com/229x158.jpg/5fa2dd/ffffff	good	50	31
92	Fleetwood	Morey	http://dummyimage.com/125x176.jpg/cc0000/ffffff	good	28	81
93	GTI	Aaron	http://dummyimage.com/184x113.png/ff4444/ffffff	bad	84	33
94	F-Series	Petkov	http://dummyimage.com/210x210.png/cc0000/ffffff	supreme	78	25
95	Vitara	Morpeth	http://dummyimage.com/156x164.bmp/dddddd/000000	supreme	81	46
96	G-Class	Urch	http://dummyimage.com/182x111.jpg/cc0000/ffffff	usable	44	23
97	Econoline E350	Grishankov	http://dummyimage.com/136x249.jpg/ff4444/ffffff	usable	36	81
98	X5	Cottee	http://dummyimage.com/163x240.png/ff4444/ffffff	supreme	55	73
99	Quattroporte	Illyes	http://dummyimage.com/198x104.jpg/5fa2dd/ffffff	bad	6	81
100	GTI	Tayspell	http://dummyimage.com/134x154.bmp/5fa2dd/ffffff	good	68	69
\.


--
-- Data for Name: barang_dikembalikan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY barang_dikembalikan (no_resi, no_urut, id_barang) FROM stdin;
10	1	95
11	2	12
7	3	58
13	4	96
9	5	74
14	6	18
14	7	5
14	8	69
4	9	79
8	10	96
11	11	17
18	12	21
6	13	72
18	14	33
15	15	79
19	16	42
7	17	99
8	18	28
1	19	8
5	20	72
17	21	87
18	22	47
19	23	89
2	24	14
19	25	46
15	26	26
13	27	10
13	28	79
14	29	41
20	30	92
11	31	17
4	32	80
4	33	76
8	34	42
7	35	93
2	36	46
2	37	63
17	38	39
9	39	66
1	40	79
\.


--
-- Data for Name: barang_dikirim; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY barang_dikirim (no_resi, no_urut, id_barang, tanggal_review, review) FROM stdin;
14	1	37	2018-04-15	Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.
3	2	85	2018-07-16	Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.
15	3	11	2018-08-06	Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.
16	4	86	2018-08-30	Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.
12	5	51	2018-07-23	Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.
1	6	16	2018-10-17	Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.
1	7	88	2018-09-05	Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.
11	8	93	2018-07-22	Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.
3	9	55	2018-11-15	Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.
12	10	3	2018-06-01	Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.
10	11	74	2018-05-23	Vestibulum sed magna at nunc commodo placerat.
2	12	22	2018-09-29	Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.
10	13	58	2018-07-20	Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.
6	14	5	2018-04-24	Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus.
20	15	28	2018-07-31	Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.
4	16	11	2018-07-01	Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus.
19	17	1	2019-02-20	In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum.
2	18	4	2018-11-29	Vivamus tortor.
5	19	88	2018-10-12	Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl.
1	20	75	2018-11-10	Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.
4	21	98	2019-04-09	In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.
15	22	55	2018-11-24	Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.
7	23	74	2019-04-12	Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.
3	24	71	2019-01-31	Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.
16	25	16	2018-08-07	Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.
2	26	29	2018-10-18	Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio.
16	27	17	2018-04-22	Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.
20	28	15	2019-01-21	Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum.
11	29	77	2018-09-18	Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.
12	30	70	2018-06-21	Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.
19	31	15	2018-11-02	Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.
1	32	97	2018-06-24	Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.
17	33	83	2018-04-15	Cras in purus eu magna vulputate luctus.
11	34	23	2018-09-17	Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.
15	35	47	2019-02-15	Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.
19	36	22	2019-03-20	Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.
16	37	18	2019-02-02	Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.
12	38	4	2019-04-07	Nullam molestie nibh in lectus. Pellentesque at nulla.
19	39	93	2019-01-01	Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.
8	40	23	2018-05-19	Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.
\.


--
-- Data for Name: barang_pesanan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY barang_pesanan (id_pemesanan, no_urut, id_barang, tanggal_sewa, lama_sewa, tanggal_kembali, status) FROM stdin;
41	1	57	2018-05-04	22	2019-04-13	sudah dikembalikan
26	2	61	2018-05-12	20	2018-09-09	dalam masa sewa
5	3	31	2018-05-12	28	2019-04-11	batal
46	4	68	2018-04-14	22	2018-07-10	batal
14	5	77	2018-05-02	27	2018-08-21	dalam masa sewa
24	6	59	2018-04-28	23	2019-06-11	dalam masa sewa
13	7	53	2018-04-27	30	2018-09-25	sedang dikonfirmasi
41	8	30	2018-04-29	18	2018-10-17	menunggu pembayaran
30	9	75	2018-04-14	20	2018-10-22	menunggu pembayaran
13	10	77	2018-05-03	30	2019-03-05	batal
18	11	82	2018-05-08	28	2019-02-24	sedang dikirim
49	12	28	2018-05-11	25	2019-05-03	sedang dikirim
4	13	41	2018-04-14	27	2018-11-18	sudah dikembalikan
23	14	33	2018-04-18	27	2018-12-05	dalam masa sewa
9	15	77	2018-04-18	24	2018-12-28	dalam masa sewa
48	16	62	2018-04-16	21	2018-05-25	batal
23	17	86	2018-05-04	26	2018-11-19	sedang dikirim
3	18	20	2018-05-07	30	2018-08-25	menunggu pembayaran
13	19	11	2018-04-19	25	2019-01-18	dalam masa sewa
33	20	16	2018-04-17	25	2018-10-13	sedang dikirim
36	21	8	2018-04-29	30	2018-06-10	dalam masa sewa
50	22	83	2018-05-11	19	2018-11-20	menunggu pembayaran
36	23	96	2018-04-14	24	2018-12-17	sudah dikembalikan
37	24	79	2018-04-15	29	2019-01-28	sudah dikembalikan
38	25	34	2018-05-07	28	2019-03-14	batal
47	26	5	2018-04-30	22	2018-07-13	batal
24	27	87	2018-04-23	20	2018-09-21	menunggu pembayaran
18	28	10	2018-04-22	17	2018-10-15	sudah dikembalikan
25	29	48	2018-04-22	24	2018-09-26	batal
28	30	41	2018-05-04	28	2019-02-14	sedang dikonfirmasi
5	31	88	2018-04-25	19	2018-10-17	sedang dikirim
30	32	88	2018-04-22	18	2018-09-29	sedang dikonfirmasi
6	33	22	2018-04-25	21	2018-09-09	batal
43	34	67	2018-04-27	19	2018-12-29	dalam masa sewa
13	35	98	2018-04-28	25	2019-04-15	menunggu pembayaran
48	36	77	2018-05-08	29	2019-02-07	sedang dikirim
18	37	13	2018-05-02	22	2018-07-07	menunggu pembayaran
10	38	81	2018-05-13	26	2018-12-08	sedang dikonfirmasi
44	39	7	2018-05-13	20	2019-03-06	sedang dikonfirmasi
43	40	95	2018-05-09	25	2019-05-10	sudah dikembalikan
31	41	42	2018-05-13	21	2018-11-15	sedang dikirim
16	42	67	2018-05-11	30	2019-06-10	sudah dikembalikan
27	43	64	2018-05-11	24	2019-04-18	batal
18	44	73	2018-04-28	28	2019-05-16	sedang dikirim
30	45	23	2018-04-15	29	2019-04-11	sedang dikirim
2	46	53	2018-05-01	26	2019-04-19	sedang dikonfirmasi
27	47	37	2018-05-04	17	2018-07-21	sedang dikirim
13	48	90	2018-05-13	19	2018-06-11	sedang dikonfirmasi
11	49	28	2018-04-18	27	2019-06-02	dalam masa sewa
6	50	12	2018-05-01	20	2018-10-08	menunggu pembayaran
8	51	33	2018-04-30	26	2018-12-26	sedang dikonfirmasi
40	52	17	2018-05-09	29	2018-06-08	batal
21	53	59	2018-05-12	22	2018-06-20	dalam masa sewa
27	54	91	2018-05-04	23	2019-05-12	menunggu pembayaran
16	55	47	2018-04-14	30	2018-08-25	dalam masa sewa
15	56	52	2018-05-06	19	2018-08-18	sedang dikonfirmasi
15	57	59	2018-04-19	29	2018-05-25	sedang dikonfirmasi
17	58	50	2018-04-29	25	2019-03-24	sedang dikonfirmasi
12	59	37	2018-05-04	23	2018-08-22	menunggu pembayaran
29	60	14	2018-05-11	24	2018-08-12	sudah dikembalikan
47	61	34	2018-04-24	28	2018-06-06	sedang dikonfirmasi
13	62	91	2018-05-13	24	2018-12-08	sedang dikonfirmasi
38	63	32	2018-05-13	29	2019-05-03	sedang dikirim
17	64	37	2018-05-12	22	2019-05-09	batal
42	65	24	2018-05-03	27	2018-10-21	menunggu pembayaran
17	66	34	2018-05-09	20	2019-02-13	sedang dikonfirmasi
47	67	25	2018-04-19	15	2019-01-07	menunggu pembayaran
31	68	27	2018-04-23	30	2019-03-13	dalam masa sewa
12	69	83	2018-04-25	21	2018-12-22	dalam masa sewa
18	70	95	2018-05-10	27	2018-12-21	sedang dikonfirmasi
41	71	67	2018-04-27	25	2018-06-08	sedang dikirim
22	72	16	2018-05-03	19	2018-07-16	sedang dikonfirmasi
18	73	63	2018-05-03	23	2019-01-28	dalam masa sewa
21	74	95	2018-05-09	19	2018-08-03	menunggu pembayaran
42	75	47	2018-04-28	17	2019-02-11	sudah dikembalikan
8	76	30	2018-05-01	21	2018-10-13	sedang dikirim
37	77	75	2018-05-13	22	2019-05-20	sedang dikonfirmasi
19	78	97	2018-05-12	29	2018-09-20	sedang dikirim
5	79	5	2018-04-15	19	2019-01-10	sedang dikirim
47	80	88	2018-04-27	26	2019-01-30	menunggu pembayaran
27	81	40	2018-05-10	29	2019-05-27	dalam masa sewa
50	82	8	2018-04-30	19	2018-11-25	sedang dikirim
30	83	94	2018-05-04	28	2018-11-18	sedang dikonfirmasi
16	84	72	2018-05-01	22	2019-06-10	sedang dikonfirmasi
9	85	86	2018-05-11	30	2018-10-08	sudah dikembalikan
35	86	97	2018-04-24	19	2019-04-27	batal
36	87	97	2018-05-11	17	2018-07-24	batal
19	88	2	2018-04-16	21	2018-05-31	batal
13	89	85	2018-04-22	30	2019-02-19	dalam masa sewa
28	90	6	2018-04-18	24	2019-04-21	sedang dikirim
47	91	99	2018-05-11	16	2018-08-21	sedang dikirim
44	92	80	2018-04-14	26	2019-04-19	batal
30	93	46	2018-05-03	26	2018-09-18	sedang dikirim
50	94	61	2018-04-17	18	2018-10-08	sedang dikirim
22	95	35	2018-04-18	26	2019-03-02	batal
5	96	9	2018-04-27	21	2019-03-06	batal
31	97	38	2018-05-10	26	2019-04-29	sedang dikirim
25	98	81	2018-04-21	16	2018-07-11	dalam masa sewa
12	99	29	2018-05-13	25	2019-03-06	sedang dikirim
47	100	28	2018-04-22	18	2019-02-08	sedang dikonfirmasi
\.


--
-- Data for Name: chat; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY chat (id, pesan, date_time, no_ktp_anggota, no_ktp_admin) FROM stdin;
1	Excision or destruction of other lesion of external ear	2019-04-10 22:46:30	53	94
2	Truncal vagotomy	2019-04-10 22:49:30	49	91
3	Myotomy of sigmoid colon	2019-04-10 22:52:30	25	91
4	Transabdominal endoscopy of small intestine	2019-04-10 22:55:30	46	100
5	Other and unspecified ablation of renal lesion or tissue	2019-04-10 22:58:30	90	91
6	Repair of rectocele	2019-04-10 23:01:30	7	98
7	Lysis of posterior synechiae	2019-04-10 23:04:30	45	97
8	Suprapubic sling operation	2019-04-10 23:07:30	7	100
9	Unilateral repair of femoral hernia with graft or prosthesis	2019-04-10 23:10:30	76	98
10	Other reconstruction of thumb	2019-04-10 23:13:30	58	99
11	Annuloplasty	2019-04-10 23:16:30	11	92
12	Laparoscopic bilateral repair of inguinal hernia, one direct and one indirect, with graft or prosthesis	2019-04-10 23:19:30	54	91
13	Partial sialoadenectomy	2019-04-10 23:22:30	20	96
14	Radical neck dissection, not otherwise specified	2019-04-10 23:25:30	12	92
15	Thoracoscopic pleural biopsy	2019-04-10 23:28:30	26	92
16	Epididymectomy	2019-04-10 23:31:30	25	95
17	Removal of foreign body from peritoneal cavity	2019-04-10 23:34:30	30	93
18	Open reduction of dislocation of foot and toe	2019-04-10 23:37:30	34	93
19	Lung volume reduction surgery	2019-04-10 23:40:30	70	95
20	Injection or infusion of electrolytes	2019-04-10 23:43:30	41	92
21	Destruction of chorioretinal lesion by cryotherapy	2019-04-10 23:46:30	15	95
22	Pericardiocentesis	2019-04-10 23:49:30	11	96
23	Immunization for autoimmune disease	2019-04-10 23:52:30	14	96
24	Procedure on two vessels	2019-04-10 23:55:30	90	91
25	Autologous hematopoietic stem cell transplant without purging	2019-04-10 23:58:30	24	94
26	Closed reduction of fracture without internal fixation, humerus	2019-04-11 00:01:30	46	95
27	Lower limb amputation, not otherwise specified	2019-04-11 00:04:30	77	100
28	Disarticulation of elbow	2019-04-11 00:07:30	69	94
29	Skeletal x-ray of wrist and hand	2019-04-11 00:10:30	44	94
30	Microscopic examination of specimen from bladder, urethra, prostate, seminal vesicle, perivesical tissue, and of urine and semen, bacterial smear	2019-04-11 00:13:30	39	99
31	Other x-ray of fallopian tubes and uterus	2019-04-11 00:16:30	79	99
32	Other repair or reconstruction of nipple	2019-04-11 00:19:30	62	99
33	Suture of laceration of cervix	2019-04-11 00:22:30	48	97
34	Other operations on nervous system	2019-04-11 00:25:30	33	97
35	Disarticulation of ankle	2019-04-11 00:28:30	59	96
36	Multi-source photon radiosurgery	2019-04-11 00:31:30	8	98
37	Other computerized axial tomography	2019-04-11 00:34:30	37	94
38	Injection or instillation of radioisotopes	2019-04-11 00:37:30	35	92
39	Microscopic examination of specimen from unspecified site, toxicology	2019-04-11 00:40:30	32	97
40	Renal scan and radioisotope function study	2019-04-11 00:43:30	76	99
41	Insertion of permanent pacemaker, initial or replacement, type of device not specified	2019-04-11 00:46:30	65	98
42	Vaccination against tuberculosis	2019-04-11 00:49:30	85	96
43	Other computer assisted surgery	2019-04-11 00:52:30	62	98
44	Gastric cooling	2019-04-11 00:55:30	56	96
45	Endoscopic insertion of nasobiliary drainage tube	2019-04-11 00:58:30	89	92
46	Spinal blood patch	2019-04-11 01:01:30	16	91
47	Other free graft to conjunctiva	2019-04-11 01:04:30	64	92
48	Incision of bronchus	2019-04-11 01:07:30	65	96
49	Endoscopic excision or destruction of lesion of duodenum	2019-04-11 01:10:30	8	96
50	Ligation of thyroid vessels	2019-04-11 01:13:30	9	100
\.


--
-- Data for Name: info_barang_level; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY info_barang_level (id_barang, nama_level, harga_sewa, porsi_royalti) FROM stdin;
1	Silver	30805	17
2	Silver	35128	17
3	Silver	36174	19
4	Silver	39985	18
5	Silver	36914	13
6	Silver	33296	10
7	Silver	34499	19
8	Silver	31422	16
9	Silver	37538	18
10	Silver	35210	13
11	Silver	38932	12
12	Silver	32593	13
13	Silver	30638	12
14	Silver	31412	12
15	Silver	37585	19
16	Silver	32933	18
17	Silver	32951	18
18	Silver	31095	17
19	Silver	31772	18
20	Silver	36369	13
21	Silver	36033	16
22	Silver	30240	20
23	Silver	35063	17
24	Silver	34225	20
25	Silver	31301	12
26	Silver	39206	15
27	Silver	32649	20
28	Silver	37048	14
29	Silver	39914	13
30	Silver	37058	11
31	Silver	39972	10
32	Silver	36100	16
33	Silver	33703	15
34	Silver	33382	18
35	Silver	33215	14
36	Silver	30194	13
37	Silver	32692	13
38	Silver	32276	11
39	Silver	35045	20
40	Silver	38054	10
41	Silver	34999	18
42	Silver	33216	11
43	Silver	36346	18
44	Silver	39116	19
45	Silver	34781	20
46	Silver	35096	19
47	Silver	38086	18
48	Silver	34708	12
49	Silver	30279	14
50	Silver	34466	17
51	Silver	34309	15
52	Silver	33369	11
53	Silver	39664	12
54	Silver	32880	16
55	Silver	39533	19
56	Silver	36108	18
57	Silver	38527	11
58	Silver	35757	10
59	Silver	38723	11
60	Silver	35433	16
61	Silver	38605	19
62	Silver	34615	17
63	Silver	30797	16
64	Silver	39415	19
65	Silver	35979	14
66	Silver	33974	15
67	Silver	31193	17
68	Silver	30512	20
69	Silver	34801	16
70	Silver	32505	18
71	Silver	32487	16
72	Silver	39912	10
73	Silver	31194	12
74	Silver	34688	10
75	Silver	38189	20
76	Silver	34022	11
77	Silver	35462	11
78	Silver	30044	15
79	Silver	32679	14
80	Silver	30208	18
81	Silver	31818	14
82	Silver	32004	13
83	Silver	35919	16
84	Silver	36737	14
85	Silver	32101	13
86	Silver	36182	15
87	Silver	39891	15
88	Silver	39090	18
89	Silver	35581	18
90	Silver	35730	13
91	Silver	37235	11
92	Silver	35757	13
93	Silver	37563	11
94	Silver	35661	15
95	Silver	38951	11
96	Silver	33980	19
97	Silver	33714	14
98	Silver	30302	18
99	Silver	37823	12
100	Silver	33173	12
1	Gold	40530	23
2	Gold	45454	21
3	Gold	49791	25
4	Gold	45678	21
5	Gold	41583	21
6	Gold	46179	28
7	Gold	46862	28
8	Gold	49172	28
9	Gold	40106	30
10	Gold	46259	24
11	Gold	41533	24
12	Gold	49353	26
13	Gold	43783	28
14	Gold	42250	30
15	Gold	43876	30
16	Gold	48188	28
17	Gold	41433	28
18	Gold	45833	23
19	Gold	40927	28
20	Gold	45687	29
21	Gold	40516	29
22	Gold	45397	27
23	Gold	42640	23
24	Gold	40650	30
25	Gold	46660	24
26	Gold	49941	21
27	Gold	43252	29
28	Gold	45729	20
29	Gold	42895	21
30	Gold	47695	21
31	Gold	41381	25
32	Gold	49005	29
33	Gold	46615	20
34	Gold	44851	28
35	Gold	41701	30
36	Gold	49035	27
37	Gold	46335	30
38	Gold	49628	28
39	Gold	43981	28
40	Gold	41213	27
41	Gold	45457	24
42	Gold	48619	26
43	Gold	44757	21
44	Gold	49801	24
45	Gold	48932	28
46	Gold	41977	22
47	Gold	48754	21
48	Gold	41434	20
49	Gold	44464	21
50	Gold	44773	30
51	Gold	43484	27
52	Gold	45090	26
53	Gold	46771	25
54	Gold	46925	23
55	Gold	48835	27
56	Gold	40029	25
57	Gold	45560	25
58	Gold	42298	22
59	Gold	42304	29
60	Gold	40632	21
61	Gold	41322	30
62	Gold	40676	27
63	Gold	47367	29
64	Gold	40262	22
65	Gold	40096	24
66	Gold	44818	27
67	Gold	42912	30
68	Gold	49761	24
69	Gold	48639	25
70	Gold	49603	29
71	Gold	47186	25
72	Gold	45560	20
73	Gold	40526	27
74	Gold	44719	30
75	Gold	46974	29
76	Gold	46496	22
77	Gold	45786	29
78	Gold	41193	25
79	Gold	46218	20
80	Gold	45073	30
81	Gold	46960	29
82	Gold	48736	22
83	Gold	47571	27
84	Gold	46982	28
85	Gold	45694	28
86	Gold	47140	23
87	Gold	40280	26
88	Gold	48482	23
89	Gold	45747	20
90	Gold	46212	23
91	Gold	46849	29
92	Gold	43053	21
93	Gold	40166	27
94	Gold	43876	23
95	Gold	48458	25
96	Gold	42469	22
97	Gold	48579	30
98	Gold	42274	28
99	Gold	43993	20
100	Gold	44777	23
1	Bronze	24962	2
2	Bronze	20022	2
3	Bronze	26960	1
4	Bronze	28706	3
5	Bronze	21739	3
6	Bronze	20178	6
7	Bronze	23767	5
8	Bronze	24583	8
9	Bronze	25535	9
10	Bronze	26621	7
11	Bronze	29842	7
12	Bronze	23707	7
13	Bronze	21391	10
14	Bronze	23242	5
15	Bronze	26419	6
16	Bronze	20949	9
17	Bronze	27098	4
18	Bronze	20656	9
19	Bronze	22103	2
20	Bronze	20927	4
21	Bronze	28469	4
22	Bronze	20074	10
23	Bronze	22448	2
24	Bronze	21412	10
25	Bronze	22905	7
26	Bronze	29081	4
27	Bronze	28092	9
28	Bronze	29175	8
29	Bronze	20128	8
30	Bronze	24823	1
31	Bronze	27375	10
32	Bronze	29392	8
33	Bronze	26966	9
34	Bronze	24499	1
35	Bronze	24293	8
36	Bronze	23155	7
37	Bronze	27210	10
38	Bronze	27286	7
39	Bronze	22671	3
40	Bronze	22120	7
41	Bronze	25509	3
42	Bronze	28363	8
43	Bronze	24987	5
44	Bronze	27462	5
45	Bronze	26758	1
46	Bronze	20033	7
47	Bronze	22033	4
48	Bronze	25312	1
49	Bronze	24855	4
50	Bronze	21709	5
51	Bronze	25187	6
52	Bronze	29662	5
53	Bronze	23827	2
54	Bronze	24673	6
55	Bronze	29185	2
56	Bronze	26271	9
57	Bronze	24814	6
58	Bronze	24621	8
59	Bronze	29893	9
60	Bronze	20959	3
61	Bronze	25339	9
62	Bronze	27650	6
63	Bronze	24808	10
64	Bronze	28678	2
65	Bronze	24554	1
66	Bronze	22975	1
67	Bronze	29617	2
68	Bronze	25528	10
69	Bronze	27580	10
70	Bronze	24416	1
71	Bronze	29459	3
72	Bronze	27608	3
73	Bronze	23244	2
74	Bronze	25223	9
75	Bronze	28945	1
76	Bronze	20302	4
77	Bronze	27862	2
78	Bronze	26492	2
79	Bronze	23474	5
80	Bronze	22355	8
81	Bronze	21907	1
82	Bronze	22541	9
83	Bronze	22868	3
84	Bronze	21350	4
85	Bronze	23813	6
86	Bronze	27394	5
87	Bronze	20632	9
88	Bronze	20236	1
89	Bronze	21900	10
90	Bronze	20315	3
91	Bronze	22237	7
92	Bronze	25362	3
93	Bronze	25781	5
94	Bronze	21889	9
95	Bronze	22057	3
96	Bronze	29461	5
97	Bronze	27727	7
98	Bronze	29377	8
99	Bronze	24143	10
100	Bronze	27820	9
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY item (nama, deskripsi, usia_dari, usia_sampai, bahan) FROM stdin;
Fleetwood	Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.	19	25	Trifolium polymorphum Poir.
TSX	Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.	4	30	Halenia Borkh.
Land Cruiser	Phasellus in felis. Donec semper sapien a libero. Nam dui.	17	46	Cynoglossum virginianum L. var. virginianum
Vitara	Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.	3	40	Caesalpinia phyllanthoides Standl.
X5	Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.	12	41	Anthriscus sylvestris (L.) Hoffm.
Tundra	Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.	17	50	Vernonia baldwinii Torr. ssp. interior (Small) Faust
3 Series	Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.	5	45	Sidalcea pedata A. Gray
Legacy	Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.	21	50	Origanum maru L.
Esprit	Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.	9	45	Lasianthus lanceolatus (Griseb.) G. Maza
Impreza	Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.	7	38	Phymatosorus scolopendria (Burm. f.) Pic. Serm.
GTI	Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.	25	43	Argentina egedii (Wormsk.) Rydb. ssp. egedii
G-Class	Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.	24	29	Centratherum Cass.
Grand Prix	Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.	24	49	Adiantum villosum L.
Econoline E350	Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.	12	25	Loranthus L.
Quattroporte	Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.	15	45	Crotalaria cunninghamii R. Br.
Tucson	Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.	24	50	Paeonia delavayi Franch.
TL	Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.	16	37	Eugenia procera (Sw.) Poir.
H2	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.	19	26	Aloina brevirostris (Hook. & Grev.) Kindb.
Exige	In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.	12	45	Argemone munita Durand & Hilg.
Firebird	Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.	18	37	Desmodium cubense Griseb.
Villager	In congue. Etiam justo. Etiam pretium iaculis justo.	16	34	Lecania dubitans (Nyl.) A.L. Sm.
F-Series	Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.	3	45	Beta nana Boiss. & Heldr.
Corvette	Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.	24	28	Corydalis caseana A. Gray
Vibe	In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.	4	30	Kennedia prostrata R. Br.
A5	Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.	16	44	Calochortus kennedyi Porter var. kennedyi
\.


--
-- Data for Name: kategori; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY kategori (nama, level, sub_dari) FROM stdin;
Motorik	1	\N
Mobilik	1	\N
Kapalik	1	\N
Mobil-Mobilan	2	Mobilik
Mobil Mahal	3	Mobil-Mobilan
\.


--
-- Data for Name: kategori_item; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY kategori_item (nama_item, nama_kategori) FROM stdin;
Econoline E350	Mobil-Mobilan
Econoline E350	Motorik
Econoline E350	Kapalik
Econoline E350	Mobil Mahal
Econoline E350	Mobilik
X5	Mobil-Mobilan
X5	Motorik
X5	Kapalik
X5	Mobil Mahal
X5	Mobilik
Tundra	Mobil-Mobilan
Tundra	Motorik
Tundra	Kapalik
Tundra	Mobil Mahal
Tundra	Mobilik
Vibe	Mobil-Mobilan
Vibe	Motorik
Vibe	Kapalik
Vibe	Mobil Mahal
Vibe	Mobilik
Firebird	Mobil-Mobilan
Firebird	Motorik
Firebird	Kapalik
Firebird	Mobil Mahal
Firebird	Mobilik
GTI	Mobil-Mobilan
GTI	Motorik
GTI	Kapalik
GTI	Mobil Mahal
GTI	Mobilik
H2	Mobil-Mobilan
H2	Motorik
H2	Kapalik
H2	Mobil Mahal
H2	Mobilik
Vitara	Mobil-Mobilan
Vitara	Motorik
Vitara	Kapalik
Vitara	Mobil Mahal
Vitara	Mobilik
Fleetwood	Mobil-Mobilan
Fleetwood	Motorik
Fleetwood	Kapalik
Fleetwood	Mobil Mahal
Fleetwood	Mobilik
Exige	Mobil-Mobilan
Exige	Motorik
Exige	Kapalik
Exige	Mobil Mahal
Exige	Mobilik
Quattroporte	Mobil-Mobilan
Quattroporte	Motorik
Quattroporte	Kapalik
Quattroporte	Mobil Mahal
Quattroporte	Mobilik
TSX	Mobil-Mobilan
TSX	Motorik
TSX	Kapalik
TSX	Mobil Mahal
TSX	Mobilik
Legacy	Mobil-Mobilan
Legacy	Motorik
Legacy	Kapalik
Legacy	Mobil Mahal
Legacy	Mobilik
F-Series	Mobil-Mobilan
F-Series	Motorik
F-Series	Kapalik
F-Series	Mobil Mahal
F-Series	Mobilik
TL	Mobil-Mobilan
TL	Motorik
TL	Kapalik
TL	Mobil Mahal
TL	Mobilik
\.


--
-- Data for Name: level_keanggotaan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY level_keanggotaan (nama_level, minimum_poin, deskripsi) FROM stdin;
Gold	500000	Awesome user
Bronze	0	New user
Mythical	300000	Wow You are magician
Silver	100000	Intermediate user
\.


--
-- Data for Name: pemesanan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY pemesanan (id_pemesanan, datetime_pesanan, kuantitas_barang, harga_sewa, ongkos, no_ktp_pemesan, status) FROM stdin;
2	2018-12-08 00:00:00	45	53422	7322	28	sedang dikonfirmasi
3	2018-06-05 00:00:00	44	72640	6402	76	sudah dikembalikan
4	2019-03-10 00:00:00	91	38314	5049	87	batal
5	2019-01-02 00:00:00	72	69690	1189	75	menunggu pembayaran
6	2018-05-15 00:00:00	59	\N	\N	46	sudah dikembalikan
7	2019-02-21 00:00:00	14	15283	3124	9	dalam masa sewa
8	2019-04-06 00:00:00	35	61240	5381	70	sedang dikirim
9	2018-05-08 00:00:00	98	53636	8540	50	sedang dikirim
10	2018-08-17 00:00:00	7	45928	6399	71	sudah dikembalikan
11	2019-01-14 00:00:00	35	89975	8397	64	sedang dikonfirmasi
12	2019-01-27 00:00:00	75	91487	6357	56	sudah dikembalikan
13	2018-07-17 00:00:00	6	35856	9943	62	menunggu pembayaran
14	2018-05-21 00:00:00	84	34200	5813	76	batal
15	2018-07-12 00:00:00	90	56745	6537	55	sedang dikonfirmasi
16	2018-10-03 00:00:00	69	45994	4182	15	sedang dikirim
17	2019-01-01 00:00:00	83	39500	6042	88	sudah dikembalikan
18	2018-08-18 00:00:00	69	91939	4937	41	sedang dikirim
19	2019-04-03 00:00:00	47	20738	7065	64	dalam masa sewa
20	2018-07-22 00:00:00	8	\N	\N	10	batal
21	2018-08-11 00:00:00	89	78382	8394	16	dalam masa sewa
22	2019-04-02 00:00:00	50	46716	9698	2	menunggu pembayaran
23	2019-01-30 00:00:00	100	69455	4787	86	sedang dikirim
24	2018-10-20 00:00:00	93	30072	6929	18	sedang dikonfirmasi
25	2018-04-26 00:00:00	42	72556	8195	28	dalam masa sewa
26	2018-06-29 00:00:00	21	88136	4416	87	dalam masa sewa
27	2018-09-08 00:00:00	94	60429	8699	7	sedang dikonfirmasi
28	2018-12-25 00:00:00	42	87918	5208	9	sudah dikembalikan
29	2018-08-31 00:00:00	37	59744	3527	90	sedang dikonfirmasi
30	2018-05-18 00:00:00	38	68271	1905	29	sudah dikembalikan
31	2018-07-01 00:00:00	15	83154	6033	46	dalam masa sewa
32	2018-11-06 00:00:00	45	68693	2180	59	sudah dikembalikan
33	2018-06-27 00:00:00	39	37741	8701	36	menunggu pembayaran
34	2019-03-15 00:00:00	82	90635	2880	15	batal
35	2018-10-22 00:00:00	81	47378	1481	18	sedang dikonfirmasi
36	2018-10-10 00:00:00	86	33798	3440	43	sudah dikembalikan
37	2018-09-17 00:00:00	92	16376	8562	62	sedang dikirim
38	2018-05-18 00:00:00	34	91211	7034	73	sudah dikembalikan
39	2018-07-30 00:00:00	35	76880	6825	84	sudah dikembalikan
40	2018-08-01 00:00:00	76	75055	3115	85	menunggu pembayaran
41	2018-08-02 00:00:00	63	\N	\N	88	sedang dikirim
42	2018-06-26 00:00:00	44	23016	9934	14	menunggu pembayaran
43	2018-08-05 00:00:00	79	79094	6717	16	sedang dikonfirmasi
44	2018-12-30 00:00:00	17	99420	5848	87	sedang dikirim
45	2019-03-26 00:00:00	89	38113	6088	72	dalam masa sewa
46	2019-03-31 00:00:00	76	\N	\N	64	batal
47	2018-09-27 00:00:00	35	76112	2210	46	sudah dikembalikan
48	2018-06-01 00:00:00	26	26273	6637	77	menunggu pembayaran
49	2019-04-06 00:00:00	19	58647	4964	52	sedang dikirim
50	2018-07-30 00:00:00	16	54331	6679	20	sedang dikonfirmasi
1	2019-01-07 00:00:00	9	43932	9633	81	batal
\.


--
-- Data for Name: pengembalian; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY pengembalian (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
1	48	tidak langsung	45093	2018-12-31	1	Apartment
2	10	tidak cepat	20561	2018-05-04	82	Kosan
3	16	tidak langsung	39123	2018-11-05	71	Kosan
4	42	langsung	43120	2018-11-19	73	Rumah
5	4	langsung	84955	2018-10-26	81	Toko
6	32	langsung	61690	2019-03-08	42	Kantor
7	31	tidak langsung	90395	2018-07-24	5	Toko
8	18	cepat	87788	2018-10-09	9	Kontrakan
9	12	cepat	76588	2019-04-05	6	Kosan
10	6	tidak cepat	16212	2019-03-31	69	Toko
11	2	cepat	48258	2018-05-02	86	Toko
12	35	tidak cepat	89119	2018-11-06	4	Kosan
13	9	cepat	28063	2019-03-25	15	Kontrakan
14	25	tidak cepat	11358	2018-12-04	49	Rumah
15	33	cepat	37825	2018-09-26	84	Toko
16	45	tidak langsung	86037	2018-08-26	14	Toko
17	38	express	66478	2018-06-14	82	Kosan
18	46	langsung	76807	2019-03-13	40	Kosan
19	2	tidak langsung	56896	2019-01-16	54	Kosan
20	7	cepat	69312	2019-04-07	40	Kosan
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY pengguna (no_ktp, nama_lengkap, tanggal_lahir, no_telp) FROM stdin;
1	Marylin Blackaby	2019-01-22	+7 314 986 6639
2	Lory Ableson	2018-12-25	+66 838 513 6162
3	Shaine Gard	2019-01-04	+66 477 961 0176
4	Lou Devons	2018-08-10	+63 497 145 4697
5	Irwinn Johanning	2018-09-12	+62 434 291 2537
6	Mal Pitkethly	2018-10-13	+967 209 799 9653
7	Bili Connechy	2018-04-23	+54 241 424 3516
8	Bale Beresford	2018-05-03	+380 469 982 2681
9	Shea Stille	2019-02-24	+33 439 362 2498
10	Bron Beton	2018-05-17	+63 385 419 8987
11	Mata Stockton	2018-06-15	+86 806 539 9282
12	Bel Alday	2018-08-11	+86 531 376 0966
13	Sorcha Fritzer	2018-06-23	+502 174 429 6098
14	Noelle Drohun	2019-01-04	+60 272 939 4029
15	Sigismond Bosman	2018-04-28	+86 509 866 9454
16	Lara Spurrett	2018-09-12	+63 942 630 7674
17	Sebastiano Measham	2019-01-26	+86 249 414 0321
18	Rolf Jurick	2018-08-13	+254 309 597 8769
19	Beth Docwra	2019-03-30	+66 217 248 2690
20	Bobbe Audsley	2019-02-12	+992 743 947 3351
21	Ryan De Lasci	2018-10-19	+58 582 679 5061
22	Kerby Shoulder	2018-10-18	+62 194 443 5705
23	Pammie Cullington	2018-12-09	+55 162 719 1416
24	Lonnie Rymmer	2018-09-01	+55 892 707 8150
25	Mersey Frankel	2018-09-05	+63 732 659 8935
26	Gerrie Schneidau	2018-06-15	+55 662 861 3712
27	Norah Tirte	2019-01-27	+7 122 883 4060
28	Dorette McCheyne	2018-12-20	+7 440 952 4069
29	Virgil Trenbey	2018-07-01	+54 294 769 8589
30	Danya Hrihorovich	2018-04-20	+62 860 251 0797
31	Lewiss Musk	2018-04-11	+225 736 257 2336
32	Rossy Beaushaw	2018-04-28	+86 399 162 3537
33	Corene Farish	2018-04-12	+62 752 978 8640
34	Elga Spours	2018-12-17	+86 983 686 2315
35	Amaleta Webben	2019-03-04	+62 402 402 0295
36	Nicole Sawer	2018-08-13	+98 510 854 1929
37	Gardie Bernardon	2018-08-08	+7 115 102 9280
38	Jenine Wyley	2019-01-11	+86 343 251 2728
39	Marja Thebes	2018-07-11	+82 737 104 8299
40	Alonso Wenman	2018-10-11	+963 690 615 7412
41	Gisela Stampe	2018-09-05	+86 308 171 9246
42	Matt Carradice	2018-06-17	+62 750 284 2676
43	Nev Pietroni	2018-08-20	+55 544 739 0281
44	Geri Kibbel	2018-06-29	+86 660 228 5879
45	Kiersten Eltringham	2018-07-08	+41 976 428 9735
46	Evvy Louth	2018-12-29	+62 992 407 5026
47	Ryon Fuller	2018-10-15	+1 481 655 2969
48	Meridith Vicarey	2018-07-22	+66 472 794 7695
49	Quill Govinlock	2019-02-03	+351 338 243 4395
50	Donnajean Foyle	2018-10-02	+880 835 631 7570
51	Honor Rockliffe	2019-03-27	+92 725 404 5681
52	Reed Salmond	2018-07-16	+31 853 208 2593
53	Saxon Kauscher	2018-09-15	+7 236 259 7435
54	Giordano Evelyn	2018-12-18	+63 231 880 4827
55	Gilberto Lowery	2019-01-28	+7 815 454 0731
56	Domeniga Pestell	2018-06-16	+57 730 897 5993
57	Ofelia MacGahy	2019-04-06	+502 505 436 7444
58	Kara-lynn Hedan	2019-02-16	+86 637 757 8515
59	Alysa Durnall	2018-08-29	+62 339 144 6679
60	Florence Alentyev	2018-11-20	+7 180 485 2226
61	Sanford Darlow	2018-10-08	+86 143 258 0884
62	Alexandr Mollison	2018-06-05	+33 628 681 8704
63	Neville Housam	2018-12-25	+86 499 502 4814
64	Earl Crepel	2018-10-19	+33 271 965 7781
65	Tristam Younger	2018-09-30	+92 640 829 4375
66	Lynett Moiser	2018-12-22	+62 188 479 0519
67	Camilla Cartmale	2018-06-24	+62 430 919 3394
68	Lydie Ryles	2018-09-14	+30 654 863 9563
69	Kat Lineen	2018-12-07	+974 730 208 2179
70	Esther Mathet	2019-02-12	+46 701 396 0662
71	Culley Blanch	2019-02-01	+1 558 996 7132
72	Dillon Leyre	2018-10-01	+351 624 511 1186
73	Benoite Gantlett	2018-08-19	+63 667 999 7007
74	Sisile Durrett	2018-08-23	+63 578 376 9776
75	Nichols Bouch	2018-09-28	+7 335 270 4473
76	Anni Harle	2018-06-01	+54 189 963 0750
77	Gibb Fulford	2018-07-24	+7 989 519 1409
78	Leticia Borrell	2018-08-22	+850 810 463 0221
79	Cari MacAndrew	2019-01-06	+63 657 392 8099
80	Rickert Bausmann	2018-05-17	+86 358 436 4578
81	Haven Jensen	2018-11-14	+86 764 547 7109
82	Berkley Batthew	2019-03-01	+57 193 927 0182
83	Barnie Brewis	2018-07-03	+66 512 432 4940
84	Lara MacNeely	2019-01-25	+63 296 134 9263
85	Rafaela Congram	2018-12-18	+7 217 723 5628
86	Alon Brownlea	2018-10-31	+46 147 450 0695
87	Tyler Margram	2018-12-06	+66 283 837 7050
88	Ashly Ringe	2018-04-14	+420 685 233 0501
89	Marquita Stanlike	2018-05-29	+48 569 986 9523
90	Leonanie Fitzmaurice	2018-06-12	+351 957 330 2555
91	Rustie Dendon	2018-08-26	+46 296 106 9485
92	Regine Saveall	2018-11-23	+86 558 838 3088
93	Elliott Roach	2018-10-07	+63 688 227 9521
94	Chan Mourbey	2018-12-25	+55 879 804 4088
95	Garek Robb	2018-07-24	+7 724 443 6181
96	Millie Stannion	2018-05-30	+53 732 999 8891
97	Joanna Daleman	2018-06-10	+62 484 252 9936
98	Leonora Sondland	2018-04-29	+240 455 431 0632
99	Casey Moy	2018-10-01	+63 841 331 0942
100	Rene Govier	2018-08-10	+63 638 636 6478
3679213	Jaka Sembung	1999-01-20	+64 8292 8231
3679212	Lebowski	2001-08-01	+64 9999 0909
3679211	Kisman Langit	1995-11-10	+64 0012 7273
\.


--
-- Data for Name: pengiriman; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY pengiriman (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
1	38	tidak cepat	88740	2018-07-31	52	Toko
2	30	tidak cepat	29069	2018-10-29	50	Kantor
3	40	cepat	48962	2019-03-10	35	Kontrakan
4	48	express	55575	2019-02-09	40	Kosan
5	31	cepat	41109	2018-08-06	42	Kantor
6	23	cepat	62894	2018-06-19	62	Kontrakan
7	14	langsung	90594	2018-07-26	27	Toko
8	35	tidak langsung	34747	2018-05-13	10	Kantor
9	7	tidak cepat	34349	2018-10-12	10	Kantor
10	27	langsung	74709	2018-08-07	1	Kosan
11	10	express	68132	2018-12-25	13	Apartment
12	22	express	57591	2018-12-09	25	Kontrakan
13	24	tidak langsung	84751	2018-06-09	24	Toko
14	47	express	29041	2019-03-20	89	Kontrakan
15	50	express	82733	2018-10-17	22	Rumah
16	32	cepat	53122	2019-01-23	12	Kantor
17	24	cepat	16400	2018-12-16	62	Kontrakan
18	34	tidak cepat	58147	2018-04-30	12	Kantor
19	47	tidak langsung	95637	2018-11-16	5	Toko
20	3	langsung	48757	2019-01-20	66	Toko
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: toys_rent; Owner: db2018041
--

COPY status (nama, deskripsi) FROM stdin;
sedang dikonfirmasi	sedang dikonfirmasi
menunggu pembayaran	menunggu pembayaran
sudah dikembalikan	sudah dikembalikan
sedang dikirim	sedang dikirim
dalam masa sewa	dalam masa sewa
batal	batal
\.


--
-- Name: admin_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (no_ktp);


--
-- Name: alamat_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY alamat
    ADD CONSTRAINT alamat_pkey PRIMARY KEY (no_ktp_anggota, nama);


--
-- Name: anggota_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (no_ktp);


--
-- Name: barang_dikembalikan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_dikirim_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY barang_dikirim
    ADD CONSTRAINT barang_dikirim_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_pesanan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY barang_pesanan
    ADD CONSTRAINT barang_pesanan_pkey PRIMARY KEY (id_pemesanan, no_urut);


--
-- Name: barang_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY barang
    ADD CONSTRAINT barang_pkey PRIMARY KEY (id_barang);


--
-- Name: chat_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY chat
    ADD CONSTRAINT chat_pkey PRIMARY KEY (id);


--
-- Name: info_barang_level_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY info_barang_level
    ADD CONSTRAINT info_barang_level_pkey PRIMARY KEY (id_barang, nama_level);


--
-- Name: item_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY item
    ADD CONSTRAINT item_pkey PRIMARY KEY (nama);


--
-- Name: kategori_item_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY kategori_item
    ADD CONSTRAINT kategori_item_pkey PRIMARY KEY (nama_item, nama_kategori);


--
-- Name: kategori_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (nama);


--
-- Name: level_keanggotaan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY level_keanggotaan
    ADD CONSTRAINT level_keanggotaan_pkey PRIMARY KEY (nama_level);


--
-- Name: pemesanan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY pemesanan
    ADD CONSTRAINT pemesanan_pkey PRIMARY KEY (id_pemesanan);


--
-- Name: pengembalian_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY pengembalian
    ADD CONSTRAINT pengembalian_pkey PRIMARY KEY (no_resi);


--
-- Name: pengguna_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (no_ktp);


--
-- Name: pengiriman_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY pengiriman
    ADD CONSTRAINT pengiriman_pkey PRIMARY KEY (no_resi);


--
-- Name: status_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018041; Tablespace: 
--

ALTER TABLE ONLY status
    ADD CONSTRAINT status_pkey PRIMARY KEY (nama);


--
-- Name: update_poin_anggota_on_barang_trigger; Type: TRIGGER; Schema: toys_rent; Owner: db2018041
--

CREATE TRIGGER update_poin_anggota_on_barang_trigger BEFORE INSERT ON barang FOR EACH ROW EXECUTE PROCEDURE update_poin_anggota_on_pemesanan();


--
-- Name: update_poin_anggota_on_pemesanan_trigger; Type: TRIGGER; Schema: toys_rent; Owner: db2018041
--

CREATE TRIGGER update_poin_anggota_on_pemesanan_trigger BEFORE INSERT ON pemesanan FOR EACH ROW EXECUTE PROCEDURE update_poin_anggota_on_pemesanan();


--
-- Name: update_user_level_when_level_update_trigger; Type: TRIGGER; Schema: toys_rent; Owner: db2018041
--

CREATE TRIGGER update_user_level_when_level_update_trigger AFTER INSERT OR DELETE OR UPDATE ON level_keanggotaan FOR EACH ROW EXECUTE PROCEDURE update_user_level_when_level_update();


--
-- Name: update_user_level_when_user_update_trigger; Type: TRIGGER; Schema: toys_rent; Owner: db2018041
--

CREATE TRIGGER update_user_level_when_user_update_trigger BEFORE INSERT OR UPDATE ON anggota FOR EACH ROW EXECUTE PROCEDURE update_user_level_when_user_update();


--
-- Name: admin_no_ktp_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES pengguna(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: alamat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY alamat
    ADD CONSTRAINT alamat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota_level_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY anggota
    ADD CONSTRAINT anggota_level_fkey FOREIGN KEY (level) REFERENCES level_keanggotaan(nama_level) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota_no_ktp_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY anggota
    ADD CONSTRAINT anggota_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES pengguna(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikembalikan_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES barang(id_barang);


--
-- Name: barang_dikirim_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY barang_dikirim
    ADD CONSTRAINT barang_dikirim_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES pengiriman(no_resi);


--
-- Name: barang_nama_item_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY barang
    ADD CONSTRAINT barang_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES item(nama);


--
-- Name: barang_no_ktp_penyewa_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY barang
    ADD CONSTRAINT barang_no_ktp_penyewa_fkey FOREIGN KEY (no_ktp_penyewa) REFERENCES anggota(no_ktp);


--
-- Name: barang_pesanan_status_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY barang_pesanan
    ADD CONSTRAINT barang_pesanan_status_fkey FOREIGN KEY (status) REFERENCES status(nama);


--
-- Name: chat_no_ktp_admin_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY chat
    ADD CONSTRAINT chat_no_ktp_admin_fkey FOREIGN KEY (no_ktp_admin) REFERENCES admin(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY chat
    ADD CONSTRAINT chat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: info_barang_level_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY info_barang_level
    ADD CONSTRAINT info_barang_level_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES barang(id_barang);


--
-- Name: info_barang_level_nama_level_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY info_barang_level
    ADD CONSTRAINT info_barang_level_nama_level_fkey FOREIGN KEY (nama_level) REFERENCES level_keanggotaan(nama_level);


--
-- Name: kategori_item_nama_item_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY kategori_item
    ADD CONSTRAINT kategori_item_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES item(nama);


--
-- Name: kategori_item_nama_kategori_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY kategori_item
    ADD CONSTRAINT kategori_item_nama_kategori_fkey FOREIGN KEY (nama_kategori) REFERENCES kategori(nama);


--
-- Name: kategori_sub_dari_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY kategori
    ADD CONSTRAINT kategori_sub_dari_fkey FOREIGN KEY (sub_dari) REFERENCES kategori(nama);


--
-- Name: pemesanan_no_ktp_pemesan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY pemesanan
    ADD CONSTRAINT pemesanan_no_ktp_pemesan_fkey FOREIGN KEY (no_ktp_pemesan) REFERENCES anggota(no_ktp);


--
-- Name: pemesanan_status_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY pemesanan
    ADD CONSTRAINT pemesanan_status_fkey FOREIGN KEY (status) REFERENCES status(nama);


--
-- Name: pengembalian_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY pengembalian
    ADD CONSTRAINT pengembalian_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES alamat(no_ktp_anggota, nama);


--
-- Name: pengiriman_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018041
--

ALTER TABLE ONLY pengiriman
    ADD CONSTRAINT pengiriman_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES alamat(no_ktp_anggota, nama);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

    # # sql.execute('''CREATE TABLE barang (
    # # id_barang character varying(10) NOT NULL,
    # # nama_item character varying(255),
    # # warna character varying(50),
    # # url_foto text,
    # # kondisi text,
    # # lama_penggunaan integer,
    # # no_ktp_penyewa character varying(20))''')
    # #sql.execute('''INSERT INTO barang values ("aaa","aaa","aaa","aaa","aa",3,"aaa")''')
    # sql.execute('''CREATE TABLE pengguna (
    # no_ktp character varying(20) NOT NULL,
    # nama_lengkap character varying(255) NOT NULL,
    # tanggal_lahir date,
    # no_telp character varying(20)
    # )''')
    # sql.execute('''INSERT INTO pengguna values("aaa","ahmad","1999-12-03","3193")''')