# Toys Rent - C13

## Overview
We are from C-13 group.
Website link: http://toys-rent1337.herokuapp.com/hello/

## Requirements
1. Terminal
2. Python3 or more
3. Pipenv
4. Pip (Python3 version or more)

## How to install Pipenv
Assuming you already have pip in your computer, then type this in terminal
```
pip install pipenv
```

## Usage
Follows these instructions:
```
pipenv shell
```
```
pipenv install 
```
```
python manage.py runserver
```